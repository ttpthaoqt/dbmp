/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100131
 Source Host           : localhost:3306
 Source Schema         : dbmp

 Target Server Type    : MySQL
 Target Server Version : 100131
 File Encoding         : 65001

 Date: 18/05/2018 16:38:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for child_info
-- ----------------------------
DROP TABLE IF EXISTS `child_info`;
CREATE TABLE `child_info`  (
  `child_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `child_order` tinyint(4) NOT NULL,
  `work_item` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `remarks` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `del_flag` tinyint(1) NOT NULL,
  PRIMARY KEY (`child_info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Child Procedure Information' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for execution_plan
-- ----------------------------
DROP TABLE IF EXISTS `execution_plan`;
CREATE TABLE `execution_plan`  (
  `execution_plan_id` int(11) NOT NULL AUTO_INCREMENT,
  `implemented_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `plan_date` datetime(0) NOT NULL,
  `start_time` datetime(0) DEFAULT NULL,
  `end_time` datetime(0) DEFAULT NULL,
  `person_in_charge` varchar(70) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `del_flag` tinyint(1) NOT NULL,
  PRIMARY KEY (`execution_plan_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Execution Plan Information' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for execution_plan_detail
-- ----------------------------
DROP TABLE IF EXISTS `execution_plan_detail`;
CREATE TABLE `execution_plan_detail`  (
  `execution_plan_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `execution_plan_id` int(11) NOT NULL,
  `group_info_id` int(11) NOT NULL,
  `child_info_id` int(11) NOT NULL,
  `work_item_command` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `start_time` datetime(0) DEFAULT NULL,
  `end_time` datetime(0) DEFAULT NULL,
  `del_flag` tinyint(1) NOT NULL,
  PRIMARY KEY (`execution_plan_detail_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Execution Plan Detail Information' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for general_purpose_list
-- ----------------------------
DROP TABLE IF EXISTS `general_purpose_list`;
CREATE TABLE `general_purpose_list`  (
  `general_purpose_list_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `order_no` tinyint(4) NOT NULL,
  `item` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `remarks` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `del_flag` tinyint(1) NOT NULL,
  PRIMARY KEY (`general_purpose_list_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'General Purpose List Information' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for general_purpose_list_type
-- ----------------------------
DROP TABLE IF EXISTS `general_purpose_list_type`;
CREATE TABLE `general_purpose_list_type`  (
  `general_purpose_list_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_abbreviation` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `type_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `remarks` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `del_flag` tinyint(1) NOT NULL,
  PRIMARY KEY (`general_purpose_list_type_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'General Purpose List Type Information' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for group_info
-- ----------------------------
DROP TABLE IF EXISTS `group_info`;
CREATE TABLE `group_info`  (
  `group_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `execution_plan_id` int(11) NOT NULL,
  `group_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `del_flag` tinyint(1) NOT NULL,
  PRIMARY KEY (`group_info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Execution Plan Group Information' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for hibernate_sequence
-- ----------------------------
DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE `hibernate_sequence`  (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Table structure for parameter_info
-- ----------------------------
DROP TABLE IF EXISTS `parameter_info`;
CREATE TABLE `parameter_info`  (
  `parameter_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_info_id` int(11) NOT NULL,
  `parameter_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `parameter_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `del_flag` tinyint(1) NOT NULL,
  PRIMARY KEY (`parameter_info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Parameter Group Information' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for parent_group
-- ----------------------------
DROP TABLE IF EXISTS `parent_group`;
CREATE TABLE `parent_group`  (
  `parent_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_info_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `del_flag` tinyint(1) NOT NULL,
  PRIMARY KEY (`parent_group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Group Information' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for parent_info
-- ----------------------------
DROP TABLE IF EXISTS `parent_info`;
CREATE TABLE `parent_info`  (
  `parent_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_no` int(11) NOT NULL,
  `parent_name` varchar(70) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `del_flag` tinyint(1) NOT NULL,
  PRIMARY KEY (`parent_info_id`) USING BTREE,
  UNIQUE INDEX `parent_no`(`parent_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Parent procedure Information' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info`  (
  `user_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(70) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(70) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `authority` tinyint(4) NOT NULL,
  `server_id` varchar(70) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `server_password` varchar(70) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `user_name` varchar(70) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `del_flag` tinyint(1) NOT NULL,
  PRIMARY KEY (`user_info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'User Information' ROW_FORMAT = Compact;

-- ----------------------------
-- View structure for parentinfo_childinfo
-- ----------------------------
DROP VIEW IF EXISTS `parentinfo_childinfo`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `parentinfo_childinfo` AS select `p`.`parent_info_id` AS `parent_info_id`,`p`.`parent_no` AS `parent_no`,`p`.`parent_name` AS `parent_name`,`c`.`child_info_id` AS `child_info_id`,`c`.`child_order` AS `child_order`,`c`.`work_item` AS `work_item`,`c`.`remarks` AS `remarks`,`c`.`del_flag` AS `del_flag` from (`dbmp`.`parent_info` `p` left join `dbmp`.`child_info` `c` on(((`p`.`parent_info_id` = `c`.`parent_id`) and (`c`.`del_flag` = 0)))) where (`p`.`del_flag` = 0) order by `p`.`parent_info_id`,`c`.`child_order` ;

-- ----------------------------
-- View structure for viewgroupdone
-- ----------------------------
DROP VIEW IF EXISTS `viewgroupdone`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `viewgroupdone` AS SELECT b.execution_plan_id, b.group_info_id, COUNT(b.group_info_id ) AS ep_done FROM execution_plan_detail b WHERE b.del_flag=0 AND b.start_time IS NOT NULL AND b.end_time IS NOT NULL GROUP BY b.execution_plan_id, b.group_info_id ;

-- ----------------------------
-- View structure for viewgroupstatus
-- ----------------------------
DROP VIEW IF EXISTS `viewgroupstatus`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `viewgroupstatus` AS SELECT a.*, CASE WHEN viewgroupdone.ep_done IS NOT NULL THEN viewgroupdone.ep_done ELSE 0 END AS ep_done, CASE WHEN viewgrouptotal.ep_total IS NOT NULL THEN viewgrouptotal.ep_total ELSE 0 END AS ep_total 
FROM `group_info` a 
LEFT JOIN viewgroupdone ON viewgroupdone.group_info_id=a.group_info_id 
LEFT JOIN viewgrouptotal ON viewgrouptotal.group_info_id=a.group_info_id
WHERE a.del_flag=0 ;

-- ----------------------------
-- View structure for viewgrouptotal
-- ----------------------------
DROP VIEW IF EXISTS `viewgrouptotal`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `viewgrouptotal` AS select `b`.`execution_plan_id` AS `execution_plan_id`,`b`.`group_info_id` AS `group_info_id`,count(`b`.`group_info_id`) AS `ep_total` from `dbmp`.`execution_plan_detail` `b` WHERE del_flag=0 group by `b`.`execution_plan_id`,`b`.`group_info_id` ;

SET FOREIGN_KEY_CHECKS = 1;
