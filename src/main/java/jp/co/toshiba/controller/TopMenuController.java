/**
 * 
 */
package jp.co.toshiba.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import jp.co.toshiba.entity.UserInfo;

/**
 * 
 *
 */
@Controller
public class TopMenuController {

	@RequestMapping(value={"/top_menu"})
    public ModelAndView index() {
		return new ModelAndView("top_menu");
    }

}
