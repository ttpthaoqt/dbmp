/**
 * 
 */
package jp.co.toshiba.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 
 *
 */
@Controller
public class ExecutionPlanDetailController {
	
	@RequestMapping(value={"/execution_plan_detail"}, method = RequestMethod.GET)
    public ModelAndView index(Model model) {	
		ModelAndView modelAndView = new ModelAndView();
//		List<ExecutionPlan> list = executionPlanService.getList();
//		modelAndView.addObject("execution_plan_detail", list);
		modelAndView.setViewName("execution_plan_detail");
        return modelAndView;
    }

}
