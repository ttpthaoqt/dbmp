/**
 * 
 */
package jp.co.toshiba.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import jp.co.toshiba.entity.ExecutionPlan;
import jp.co.toshiba.entity.ExecutionPlanDetail;
import jp.co.toshiba.entity.ParentInfo;
import jp.co.toshiba.service.ExecutionPlanDetailService;
import jp.co.toshiba.service.ExecutionPlanService;
import jp.co.toshiba.service.GroupInfoService;
import jp.co.toshiba.service.ParameterInfoService;
import jp.co.toshiba.service.ParentGroupService;

/**
 * 
 *
 */
@Controller
public class ExecutionPlanSelectionController {
	
	private ExecutionPlanService executionPlanService;
	
	@Autowired
	public void setExecutionPlanService(ExecutionPlanService executionPlanService) {
		this.executionPlanService = executionPlanService;
	}

	@RequestMapping(value={"/execution_plan_selection"}, method = RequestMethod.GET)
    public ModelAndView index(@RequestParam(required = false) Integer ptype, @RequestParam(required = false) Integer gid, @RequestParam(required = false) Integer epid, @RequestParam(required = false) Integer e) {
		if (ptype==null) {
			ptype=1;
		}
		
		if (e==null) {
			e=0;
		}
		ModelAndView modelAndView = new ModelAndView();
		//get execution_plan_details from executionPlanID
		List<ExecutionPlan> listExecutionPlans = executionPlanService.getListExePlanSearch(ptype);
		modelAndView.addObject("execution_plans", listExecutionPlans);
		System.out.println("gid = " + gid + "epid = " + epid + "e = " + e);
		modelAndView.addObject("ptype", ptype);
		modelAndView.addObject("gid", gid);
		modelAndView.addObject("epid", epid);
		modelAndView.addObject("e", e);
		modelAndView.setViewName("execution_plan_selection");
        return modelAndView;
    }
	
	@RequestMapping(value={"/execution_plan_selection"}, method = RequestMethod.POST)
    public ModelAndView search(@RequestParam(required = false) Integer ptype, @RequestParam(required = false) Integer gid, @RequestParam(required = false) Integer epid, @RequestParam(required = false) Integer e, @RequestParam String search_start_time, @RequestParam String search_end_time, @RequestParam String search_implemented_name) {
		ModelAndView modelAndView = new ModelAndView();
		DateFormat format = new SimpleDateFormat("mm/dd/yyyy");
		Date dStartDate = null;
		Date dEndDate = null;
		try {
			if (!search_start_time.equals(String.valueOf(""))) {
				dStartDate = format.parse(search_start_time);
			}
			if (!search_end_time.equals(String.valueOf(""))) {
				dEndDate = format.parse(search_end_time);
			}
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (ptype==null) {
			ptype=1;
		}
		//get execution_plan_details from executionPlanID
		List<ExecutionPlan> listExecutionPlans = executionPlanService.getListExePlanSearch(ptype, dStartDate, dEndDate, search_implemented_name);
		modelAndView.addObject("execution_plans", listExecutionPlans);
		modelAndView.addObject("ptype", ptype);
		modelAndView.addObject("gid", gid);
		modelAndView.addObject("epid", epid);
		modelAndView.addObject("e", e);
		modelAndView.addObject("dStartDate", search_start_time);
		modelAndView.addObject("dEndDate", search_end_time);
		modelAndView.addObject("search_implemented_name", search_implemented_name);		
		modelAndView.setViewName("execution_plan_selection");
        return modelAndView;
    }
	
}
