/**
 * 
 */
package jp.co.toshiba.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import jp.co.toshiba.entity.ExecutionPlan;
import jp.co.toshiba.entity.ExecutionPlanDetail;
import jp.co.toshiba.entity.GroupInfo;
import jp.co.toshiba.entity.GroupInfoStatus;
import jp.co.toshiba.entity.UserInfo;
import jp.co.toshiba.service.ExecutionPlanDetailService;
import jp.co.toshiba.service.ExecutionPlanService;
import jp.co.toshiba.service.GroupInfoService;

/**
 * 
 *
 */
@Controller
public class WorkProgressConfirmationController {
	
	private ExecutionPlanService executionPlanService;
	private ExecutionPlanDetailService executionPlanDetailService;
	private GroupInfoService groupInfoService;
	
	@Autowired
	public void setExecutionPlanService(ExecutionPlanService executionPlanService, ExecutionPlanDetailService executionPlanDetailService, 
			GroupInfoService groupInfoService) {
		this.executionPlanService = executionPlanService;
		this.executionPlanDetailService = executionPlanDetailService;
		this.groupInfoService = groupInfoService;
	}

	@RequestMapping(value={"/work_progress_confirmation"}, method = RequestMethod.GET)
	public ModelAndView index(ModelMap modelAndView, @RequestParam(required = false) Integer gid, @RequestParam(required = false) Integer epid) {
		if (epid==null) {
			// determine executionPlanID
			ExecutionPlan exeTop = executionPlanService.getExePlanTop(2);
			if (exeTop!=null) {
				epid = exeTop.getExecution_plan_id();
			} else {
				epid = 1;
			}
		}
		
		if (gid==null) {
			// determine group id
			List<GroupInfo> listGroupInfoByExpID = groupInfoService.getDetail(epid);
			if (listGroupInfoByExpID.size()>0) {
				GroupInfo groupInfoByExpID = listGroupInfoByExpID.get(0);
				gid = groupInfoByExpID.getGroup_info_id();
			} else {
				gid=1;
			}
		}
			
		//get execution_plan from executionPlanID
		ExecutionPlan executionPlan = executionPlanService.getOne(epid);
		if (executionPlan!=null) {
			modelAndView.addAttribute("execution_plan_detail", executionPlan);
			
			List<GroupInfoStatus> listGroupInfos = groupInfoService.getGroupListStatus(epid);
			modelAndView.addAttribute("group_infos", listGroupInfos);
			
			GroupInfo groupInfo = groupInfoService.getDetail(gid, epid);
			modelAndView.addAttribute("group_info", groupInfo);
			
			//get execution_plan from executionPlanID
			List<ExecutionPlanDetail> listExecutionPlanDetails = executionPlanDetailService.getDetail(gid, epid);
			modelAndView.addAttribute("execution_plan_details", listExecutionPlanDetails);
			modelAndView.addAttribute("gid", gid);
			modelAndView.addAttribute("epid", epid);
			return new ModelAndView("work_progress_confirmation", modelAndView);
		}
		return new ModelAndView("work_progress_confirmation", modelAndView);
    }
}
