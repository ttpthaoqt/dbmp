/**
 * 
 */
package jp.co.toshiba.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import jp.co.toshiba.entity.ParentInfo;
import jp.co.toshiba.service.ParentInfoService;

/**
 * 
 *
 */
@Controller
public class ParentController {
	
	@Autowired
	private ParentInfoService parentInfoService;
	
	@RequestMapping(value={"/create_parent"})
    public ModelAndView index(ModelMap model, @RequestParam(required = false) Integer pid) {
		//check to get child info for update case
		ParentInfo parentInfo = new ParentInfo();
		if (pid!=null) {
			parentInfo = parentInfoService.getOne(pid);
		}
		model.put("parentInfo", parentInfo);
		return new ModelAndView("create_parent", model);
    }
	
	@RequestMapping(value={"/create_parent"}, method=RequestMethod.POST)
    public ModelAndView createParent(ModelMap model, @RequestParam(required = false) Integer pid, @RequestParam int parent_no, @RequestParam String parent_name) {
		
		ParentInfo pInfo = new ParentInfo();
		if (pid!=0) {
			pInfo = parentInfoService.getOne(pid);
		}
		
		pInfo.setParent_no(parent_no);
		pInfo.setParent_name(parent_name);
		if (pid==0) {
			pInfo.setDel_flag((short) 0);
			pInfo = parentInfoService.save(pInfo);
		} else {
			pInfo = parentInfoService.update(pInfo);
		}
		
		if(pInfo == null) {
			model.addAttribute("errorMessage", "create_error");
		}
		return new ModelAndView("/create_parent", model);
    }
}
