/**
 * 
 */
package jp.co.toshiba.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import jp.co.toshiba.entity.ChildInfo;
import jp.co.toshiba.entity.ExecutionPlan;
import jp.co.toshiba.entity.ParentInfo;
import jp.co.toshiba.entity.VewParentChildInf;
import jp.co.toshiba.service.ChildInfoService;
import jp.co.toshiba.service.ExecutionPlanDetailService;
import jp.co.toshiba.service.ExecutionPlanService;
import jp.co.toshiba.service.GroupInfoService;
import jp.co.toshiba.service.ParentInfoService;

/**
 * 
 *
 */
@Controller
public class ChildController {
	
	@Autowired
	private ChildInfoService childInfoService;
	private ParentInfoService parentInfoService;
	
	@Autowired
	public void setExecutionPlanService(ChildInfoService childInfoService, ParentInfoService parentInfoService) {
		this.childInfoService = childInfoService;
		this.parentInfoService = parentInfoService;
	}
	
	@RequestMapping(value={"/create_child"}, method=RequestMethod.GET)
    public ModelAndView index(ModelMap model, @RequestParam(required = false) Integer cid) {
		//check to get child info for update case
		ChildInfo childInfo = new ChildInfo();
		if (cid!=null) {
			childInfo = childInfoService.getOne(cid);
		}
		model.put("childInfo", childInfo);
		List<ParentInfo> listParentInfos = parentInfoService.getList();
		model.put("listParentInfos", listParentInfos);
		return new ModelAndView("create_child", model);
    }
	
	@RequestMapping(value={"/create_child"}, method=RequestMethod.POST)
    public ModelAndView createChild(ModelMap model, @RequestParam(required = false) Integer cid, @RequestParam int parent_id, @RequestParam int child_order, @RequestParam String work_item, @RequestParam String remarks) {
		
		ChildInfo cInfo = new ChildInfo();
		if (cid!=0) {
			cInfo = childInfoService.getOne(cid);
		}	
		cInfo.setParent_id(parent_id);
		cInfo.setChild_order(child_order);
		cInfo.setWork_item(work_item);
		cInfo.setRemarks(remarks);
		
		if (cid==0) {
			cInfo.setDel_flag((short) 0);
			cInfo = childInfoService.save(cInfo);
		} else {
			cInfo = childInfoService.update(cInfo);
		}
		
		if(cInfo == null) {
			model.addAttribute("errorMessage", "create_error");
		}
		
		return new ModelAndView("/create_child", model);
    }	
}
