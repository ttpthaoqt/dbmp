/**
 * 
 */
package jp.co.toshiba.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import jp.co.toshiba.entity.ExecutionPlan;
import jp.co.toshiba.entity.ExecutionPlanDetail;
import jp.co.toshiba.entity.GroupInfo;
import jp.co.toshiba.entity.GroupInfoStatus;
import jp.co.toshiba.service.ExecutionPlanDetailService;
import jp.co.toshiba.service.ExecutionPlanService;
import jp.co.toshiba.service.GroupInfoService;

/**
 * 
 *
 */
@Controller
public class WorkExecutionController {
	
	private ExecutionPlanService executionPlanService;
	private ExecutionPlanDetailService executionPlanDetailService;
	private GroupInfoService groupInfoService;
	
	@Autowired
	public void setExecutionPlanService(ExecutionPlanService executionPlanService, ExecutionPlanDetailService executionPlanDetailService, 
			GroupInfoService groupInfoService) {
		this.executionPlanService = executionPlanService;
		this.executionPlanDetailService = executionPlanDetailService;
		this.groupInfoService = groupInfoService;
	}

	@RequestMapping(value={"/work_execution"}, method = RequestMethod.GET)
    public ModelAndView index(ModelMap modelAndView, @RequestParam(required = false) Integer gid, @RequestParam(required = false) Integer epid, @RequestParam(required = false) Integer e) {
		if (epid==null) {
			// determine executionPlanID
			ExecutionPlan exeTop = executionPlanService.getExePlanTop(1);
			if (exeTop!=null) {
				epid = exeTop.getExecution_plan_id();
			} else {
				epid = 1;
			}
		}
		
		if (gid==null) {
			// determine group id
			List<GroupInfo> listGroupInfoByExpID = groupInfoService.getDetail(epid);
			if (listGroupInfoByExpID.size()>0) {
				GroupInfo groupInfoByExpID = listGroupInfoByExpID.get(0);
				gid = groupInfoByExpID.getGroup_info_id();
			} else {
				gid=1;
			}
		}
			
		//get execution_plan from executionPlanID
		ExecutionPlan executionPlan = executionPlanService.getOne(epid);
		if (executionPlan!=null) {
			modelAndView.addAttribute("execution_plan_detail", executionPlan);
			
			List<GroupInfoStatus> listGroupInfos = groupInfoService.getGroupListStatus(epid);
			modelAndView.addAttribute("group_infos", listGroupInfos);
			
			GroupInfo groupInfo = groupInfoService.getDetail(gid, epid);
			modelAndView.addAttribute("group_info", groupInfo);
			
			//get execution_plan from executionPlanID
			List<ExecutionPlanDetail> listExecutionPlanDetails = executionPlanDetailService.getDetail(gid, epid);
			//check last detail update
			int hasdummy = 0;
			int totalPlanDetail = listExecutionPlanDetails.size();
			if (totalPlanDetail>0) {
				ExecutionPlanDetail executionPlanDetailLast = listExecutionPlanDetails.get(totalPlanDetail-1);
				if (executionPlanDetailLast.getStart_time()!=null && executionPlanDetailLast.getEnd_time()==null) {
					hasdummy = 1;
				}
			}
			modelAndView.addAttribute("execution_plan_details", listExecutionPlanDetails);
			modelAndView.addAttribute("gid", gid);
			modelAndView.addAttribute("epid", epid);
			modelAndView.addAttribute("e", e);
			modelAndView.addAttribute("hasdummy", hasdummy);
			return new ModelAndView("work_execution", modelAndView);
		}
		return new ModelAndView("work_execution", modelAndView);
    }
	
	@RequestMapping(value={"/work_execution_update"}, method = RequestMethod.POST)
    public ModelAndView update(ModelMap model, @RequestParam(required = false) Integer gid, @RequestParam(required = false) Integer epid, @RequestParam(required = false) Integer e, @RequestParam(required = false) Integer epdid, @RequestParam(required = false) Integer epdidcopy,  @RequestParam(required = false) Integer rowindex) {
		Date dDate = new Date();
		
		//get ExecutionPlanDetail to update status for ExecutionPlan
		List<ExecutionPlanDetail> listExecutionPlanDetails = executionPlanDetailService.getDetail(epid);
		int totalPlanDetail = listExecutionPlanDetails.size();
		boolean bFirst = false;
		boolean bLast = true;
		if (totalPlanDetail>0) {
			ExecutionPlanDetail executionPlanDetailFirst = listExecutionPlanDetails.get(0);
			if (executionPlanDetailFirst.getEnd_time()==null) {
				bFirst = true;
			}
		}
		
		if (epdid!=null) {
			ExecutionPlanDetail executionPlanDetailProcessing = executionPlanDetailService.getOne(epdid);
			executionPlanDetailProcessing.setEnd_time(dDate);
			executionPlanDetailProcessing = executionPlanDetailService.update(executionPlanDetailProcessing);
		}
		
		if (epdidcopy!=null) {
			ExecutionPlanDetail executionPlanDetailCopying = executionPlanDetailService.getOne(epdidcopy);
			executionPlanDetailCopying.setStart_time(dDate);
			executionPlanDetailCopying = executionPlanDetailService.update(executionPlanDetailCopying);
		}

		//check last ExecutionPlanDetail to update status for ExecutionPlan
		for (int i=0; i<totalPlanDetail; i++) {
			ExecutionPlanDetail executionPlanDetailLast = listExecutionPlanDetails.get(i);
			if (executionPlanDetailLast.getStart_time()==null || executionPlanDetailLast.getEnd_time()==null) {
				bLast = false;
				break;
			}
		}
		
		//check to update status for execution plan when the execution plan detail first is performed
		if (bFirst) {
			//update status
			ExecutionPlan executionPlan = executionPlanService.getOne(epid);
			executionPlan.setStatus(1);
			executionPlan.setStart_time(dDate);
			executionPlan = executionPlanService.update(executionPlan);
		}
		
		if (bLast) {
			//update status
			ExecutionPlan executionPlan = executionPlanService.getOne(epid);
			executionPlan.setStatus(2);
			executionPlan.setEnd_time(dDate);
			executionPlan = executionPlanService.update(executionPlan);
		}
		return null;
	}
}
