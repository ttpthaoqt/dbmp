/**
 * 
 */
package jp.co.toshiba.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import jp.co.toshiba.entity.UserInfo;
import jp.co.toshiba.service.UserService;

/**
 * @author Login
 *
 */
@Controller
public class LoginController {

	@Autowired
	private UserService userService;
    
	@RequestMapping(value={"/"})
    public ModelAndView index(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session != null) {
		    session.invalidate();
		}
		return new ModelAndView("login", "UserInfo", new UserInfo());
    }
	
	@RequestMapping(value={"/login"}, method=RequestMethod.POST)
    public ModelAndView login(HttpServletRequest request, ModelMap model, @RequestParam String user_id, @RequestParam String password) {
		if(user_id.equals("") || password.equals("")) {
			model.put("errorMessage", "login.required");
			model.put("UserInfo", new UserInfo());
			return new ModelAndView("login", model);
		}
		UserInfo isValidUser = userService.checkUserValid(user_id, password);
		if (isValidUser == null) {
			model.put("errorMessage", "login.invalid");
			model.put("UserInfo", new UserInfo());
			return new ModelAndView("login", model);

		} else {
			request.getSession().setAttribute("user_id", isValidUser.getUser_id());
			request.getSession().setAttribute("authority", isValidUser.getAuthority());
			request.getSession().setAttribute("user_name", isValidUser.getUser_name());
	        return new ModelAndView("redirect:/top_menu");
		}
    }
	
}
