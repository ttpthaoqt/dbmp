/**
 * 
 */
package jp.co.toshiba.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import jp.co.toshiba.entity.ExecutionPlan;
import jp.co.toshiba.entity.ExecutionPlanDetail;
import jp.co.toshiba.entity.GroupInfo;
import jp.co.toshiba.entity.ParameterInfo;
import jp.co.toshiba.entity.ParentGroup;
import jp.co.toshiba.service.ExecutionPlanDetailService;
import jp.co.toshiba.service.ExecutionPlanService;
import jp.co.toshiba.service.GroupInfoService;
import jp.co.toshiba.service.ParameterInfoService;
import jp.co.toshiba.service.ParentGroupService;

/**
 * 
 *
 */
@Controller
public class ExecutionPlanController {
	
	private ExecutionPlanService executionPlanService;
	private ExecutionPlanDetailService executionPlanDetailService;
	private GroupInfoService groupInfoService;
	private ParentGroupService parentGroupService;
	private ParameterInfoService parameterInfoService;
	
	@Autowired
	public void setExecutionPlanService(ExecutionPlanService executionPlanService, ExecutionPlanDetailService executionPlanDetailService, 
			GroupInfoService groupInfoService, ParentGroupService parentGroupService, ParameterInfoService parameterInfoService) {
		this.executionPlanService = executionPlanService;
		this.executionPlanDetailService = executionPlanDetailService;
		this.groupInfoService = groupInfoService;
		this.parentGroupService = parentGroupService;
		this.parameterInfoService = parameterInfoService;
	}

	@RequestMapping(value={"/execution_plan"}, method = RequestMethod.GET)
    public ModelAndView index(Model model) {
		int group_info_id = 1;//get request parameter
		int execution_plan_id = 1;
		ModelAndView modelAndView = new ModelAndView();
		
		//get group_infos from executionPlanID
		List<GroupInfo> listGroupInfos = groupInfoService.getList();
		modelAndView.addObject("group_infos", listGroupInfos);
		
		//get parent_groups from group_info_id
		List<ParentGroup> listParentGroups = parentGroupService.getDetailByGroupID(group_info_id);
		modelAndView.addObject("parent_groups", listParentGroups);
		
		//get parent_groups from group_info_id
		List<ParameterInfo> listParameterInfos = parameterInfoService.getDetailByGroupID(group_info_id);
		modelAndView.addObject("parameter_infos", listParameterInfos);
		
		//get execution_plan_details from executionPlanID
		List<ExecutionPlanDetail> listExecutionPlanDetails = executionPlanDetailService.getDetail(group_info_id, execution_plan_id);
		modelAndView.addObject("execution_plan_details", listExecutionPlanDetails);
				
		modelAndView.setViewName("execution_plan");
        return modelAndView;
    }
}
