/**
 * 
 */
package jp.co.toshiba.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import jp.co.toshiba.entity.ParentInfo;

/**
 * 
 *
 */
@Controller
public class CreateParentController {
	
	@RequestMapping(value={"/create_parent"})
    public ModelAndView index() {
		return new ModelAndView("create_parent", "ParentInfo", new ParentInfo());
    }
}
