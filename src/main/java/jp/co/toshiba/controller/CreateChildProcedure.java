/**
 * 
 */
package jp.co.toshiba.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import jp.co.toshiba.entity.ChildInfo;

/**
 * 
 *
 */
@Controller
public class CreateChildProcedure {
	
	@RequestMapping(value={"/create_child"})
    public ModelAndView index() {
		return new ModelAndView("create_child", "ChildInfo", new ChildInfo());
    }
}
