/**
 * 
 */
package jp.co.toshiba.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import jp.co.toshiba.entity.ParentInfo;
import jp.co.toshiba.entity.VewParentChildInf;
import jp.co.toshiba.service.ParentInfoService;;

/**
 * 
 *
 */
@Controller
public class ProcedureController {

	@Autowired
	private ParentInfoService parentInfoService;
	
	@ModelAttribute("parentInfo")
	public Map<Integer,String> refData() { 
		List<ParentInfo> lstParentInfo = parentInfoService.getList();
		Map< Integer, String > parentInfo = new HashMap<Integer, String>();

		for(ParentInfo obj : lstParentInfo) {
			parentInfo.put(obj.getParent_info_id(), obj.getParent_name());
		}

		return parentInfo;

	}
	 
	@RequestMapping(value={"/procedure"}, method=RequestMethod.GET)
    public ModelAndView index(ModelMap model) {
		List<VewParentChildInf> vewParentChildInf = parentInfoService.getViewList();
		model.put("parentChildInf", vewParentChildInf);
		return new ModelAndView("procedure", model);
    }
	
	@RequestMapping(value={"/procedure"}, method=RequestMethod.POST)
    public ModelAndView search(ModelMap model, @RequestParam int selectedParentInfo, @RequestParam String workItem) {
		List<VewParentChildInf> vewParentChildInf = parentInfoService.getSearchViewList(selectedParentInfo, workItem);
		model.put("parentInfoId", selectedParentInfo);
		model.put("workItem", workItem);
		model.put("parentChildInf", vewParentChildInf);
		return new ModelAndView("/procedure");
    }
}
