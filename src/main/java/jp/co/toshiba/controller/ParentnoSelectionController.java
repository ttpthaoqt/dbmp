/**
 * 
 */
package jp.co.toshiba.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import jp.co.toshiba.entity.ParentInfo;

/**
 * 
 *
 */
@Controller
public class ParentnoSelectionController {

	@RequestMapping(value={"/parentno_selection"})
    public ModelAndView index() {
		return new ModelAndView("parentno_selection", "ParentInfo", new ParentInfo());
    }
}
