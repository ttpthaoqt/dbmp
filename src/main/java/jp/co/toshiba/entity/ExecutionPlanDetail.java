/**
 * 
 */
package jp.co.toshiba.entity;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Execution Plan Detail
 */
@Entity
@Table(name = "execution_plan_detail")
public class ExecutionPlanDetail {
	
	// ------------------------
	// PRIVATE FIELDS
	// ------------------------

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int execution_plan_detail_id;
	
	@NotNull
	private int execution_plan_id;
	
	@NotNull
	private int group_info_id;

	@NotNull
	private int child_info_id;

	@NotNull
	private String work_item_command;
	
	private Date start_time;
	
	private Date end_time;
	
	@NotNull
	private short del_flag;
	
	// ------------------------
	// PUBLIC METHODS
	// ------------------------
	
	public int getExecution_plan_detail_id() {
		return execution_plan_detail_id;
	}

	public void setExecution_plan_detail_id(int execution_plan_detail_id) {
		this.execution_plan_detail_id = execution_plan_detail_id;
	}

	public int getExecution_plan_id() {
		return execution_plan_id;
	}

	public void setExecution_plan_id(int execution_plan_id) {
		this.execution_plan_id = execution_plan_id;
	}
	
	public int getGroup_info_id() {
		return group_info_id;
	}

	public void setGroup_info_id(int group_info_id) {
		this.group_info_id = group_info_id;
	}

	public int getChild_info_id() {
		return child_info_id;
	}

	public void setChild_info_id(int child_info_id) {
		this.child_info_id = child_info_id;
	}

	public String getWork_item_command() {
		return work_item_command;
	}

	public void setWork_item_command(String work_item_command) {
		this.work_item_command = work_item_command;
	}

	public Date getStart_time() {
		return start_time;
	}

	public void setStart_time(Date start_time) {
		this.start_time = start_time;
	}

	public Date getEnd_time() {
		return end_time;
	}

	public void setEnd_time(Date end_time) {
		this.end_time = end_time;
	}

	public int getDel_flag() {
		return del_flag;
	}

	public void setDel_flag(short del_flag) {
		this.del_flag = del_flag;
	}
}
