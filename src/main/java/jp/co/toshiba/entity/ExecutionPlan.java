/**
 * 
 */
package jp.co.toshiba.entity;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Execution Plan
 */
@Entity
@Table(name = "execution_plan")
 public class ExecutionPlan {
	
	// ------------------------
	// PRIVATE FIELDS
	// ------------------------

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int execution_plan_id;
	
	@NotNull
	private String implemented_name;
	
	@NotNull
	private int status;
	
	@NotNull
	private Date plan_date;
	
	private Date start_time;
	
	private Date end_time;
	
	@NotNull
	private String person_in_charge;
	
	@NotNull
	private short del_flag;
	
	// ------------------------
	// PUBLIC METHODS
	// ------------------------
	
	public int getExecution_plan_id() {
		return execution_plan_id;
	}

	public void setExecution_plan_id(int execution_plan_id) {
		this.execution_plan_id = execution_plan_id;
	}

	public String getImplemented_name() {
		return implemented_name;
	}

	public void setImplemented_name(String implemented_name) {
		this.implemented_name = implemented_name;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getPlan_date() {
		return plan_date;
	}

	public void setPlan_date(Date plan_date) {
		this.plan_date = plan_date;
	}

	public Date getStart_time() {
		return start_time;
	}

	public void setStart_time(Date start_time) {
		this.start_time = start_time;
	}

	public Date getEnd_time() {
		return end_time;
	}

	public void setEnd_time(Date end_time) {
		this.end_time = end_time;
	}

	public String getPerson_in_charge() {
		return person_in_charge;
	}

	public void setPerson_in_charge(String person_in_charge) {
		this.person_in_charge = person_in_charge;
	}

	public short getDel_flag() {
		return del_flag;
	}

	public void setDel_flag(short del_flag) {
		this.del_flag = del_flag;
	}

}
