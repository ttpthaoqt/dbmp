/**
 * 
 */
package jp.co.toshiba.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * ChildInfo
 */
@Entity
@Table(name = "child_info")
public class ChildInfo {
	
	// ------------------------
	// PRIVATE FIELDS
	// ------------------------
	  
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int child_info_id;
	
	@NotNull
	private int parent_id;
	
	@NotNull
	private Integer child_order;
	
	@NotNull
	private String work_item;
	
	@NotNull
	private String remarks;
	
	@NotNull
	private short del_flag;
	
	// ------------------------
	// PUBLIC METHODS
	// ------------------------
	
	public int getChild_info_id() {
		return child_info_id;
	}

	public void setChild_info_id(int child_info_id) {
		this.child_info_id = child_info_id;
	}

	public int getParent_id() {
		return parent_id;
	}

	public void setParent_id(int parent_id) {
		this.parent_id = parent_id;
	}

	public Integer getChild_order() {
		return child_order;
	}

	public void setChild_order(Integer child_order) {
		this.child_order = child_order;
	}

	public String getWork_item() {
		return work_item;
	}

	public void setWork_item(String work_item) {
		this.work_item = work_item;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public int getDel_flag() {
		return del_flag;
	}

	public void setDel_flag(short del_flag) {
		this.del_flag = del_flag;
	}
	
}
