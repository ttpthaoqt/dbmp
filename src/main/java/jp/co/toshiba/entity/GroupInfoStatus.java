/**
 * 
 */
package jp.co.toshiba.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Group Info Status
 */
@Entity
@Table(name = "viewgroupstatus")
public class GroupInfoStatus {
	// ------------------------
	// PRIVATE FIELDS
	// ------------------------

	@Id
	@Column(name="group_info_id", unique=true, nullable=false)
	private int group_info_id;
	
	@Column(name="execution_plan_id", nullable=false)
	private int execution_plan_id;

	@Column(name="group_name", nullable=false)
	private String group_name;
	
	@Column(name="del_flag", nullable=false)
	private short del_flag;
	
	@Column(name="ep_done", nullable=false)
	private int ep_done;
	
	@Column(name="ep_total", nullable=false)
	private int ep_total;
	
	// ------------------------
	// PUBLIC METHODS
	// ------------------------

	public int getGroup_info_id() {
		return group_info_id;
	}

	public void setGroup_info_id(int group_info_id) {
		this.group_info_id = group_info_id;
	}

	public int getExecution_plan_id() {
		return execution_plan_id;
	}

	public void setExecution_plan_id(int execution_plan_id) {
		this.execution_plan_id = execution_plan_id;
	}

	public String getGroup_name() {
		return group_name;
	}

	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}

	public short getDel_flag() {
		return del_flag;
	}

	public void setDel_flag(short del_flag) {
		this.del_flag = del_flag;
	}

	public int getEp_done() {
		return ep_done;
	}

	public void setEp_done(int ep_done) {
		this.ep_done = ep_done;
	}

	public int getEp_total() {
		return ep_total;
	}

	public void setEp_total(int ep_total) {
		this.ep_total = ep_total;
	}
}
