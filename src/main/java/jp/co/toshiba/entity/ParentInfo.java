/**
 * 
 */
package jp.co.toshiba.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * ParentInfo
 */
@Entity
@Table(name = "parent_info")
public class ParentInfo {

	// ------------------------
	// PRIVATE FIELDS
	// ------------------------
	  
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int parent_info_id;
	  
	@NotNull
	private Integer parent_no;
	  
	@NotNull
	private String parent_name;
	  
	@NotNull
	private short del_flag;
	
	// ------------------------
	// PUBLIC METHODS
	// ------------------------
	  
	public int getParent_info_id() {
		return parent_info_id;
	}

	public void setParent_info_id(int parent_info_id) {
		this.parent_info_id = parent_info_id;
	}

	public Integer getParent_no() {
		return parent_no;
	}

	public void setParent_no(Integer parent_no) {
		this.parent_no = parent_no;
	}

	public String getParent_name() {
		return parent_name;
	}

	public void setParent_name(String parent_name) {
		this.parent_name = parent_name;
	}

	public short getDel_flag() {
		return del_flag;
	}

	public void setDel_flag(short del_flag) {
		this.del_flag = del_flag;
	}
}
