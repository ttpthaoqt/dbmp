package jp.co.toshiba.entity;

public class VewParentChildInf {
	
private int parent_info_id;

private int parent_no;

private String parent_name;

private Integer child_info_id;

private Byte child_order;

private String work_item;

private String remarks;


public VewParentChildInf(int parent_info_id, int parent_no, String parent_name, Integer child_info_id, Byte child_order, String work_item, String remarks) {
	super();
	this.parent_info_id = parent_info_id;
	this.parent_no = parent_no;
	this.parent_name = parent_name;
	this.child_info_id = child_info_id;
	this.child_order = child_order;
	this.work_item = work_item;
	this.remarks = remarks;
}

public int getParent_info_id() {
	return parent_info_id;
}

public void setParent_info_id(int parent_info_id) {
	this.parent_info_id = parent_info_id;
}

public Integer getParent_no() {
	return parent_no;
}

public void setParent_no(int parent_no) {
	this.parent_no = parent_no;
}

public String getParent_name() {
	return parent_name;
}

public void setParent_name(String parent_name) {
	this.parent_name = parent_name;
}

public Integer getChild_info_id() {
	return child_info_id;
}

public void setChild_info_id(Integer child_info_id) {
	this.child_info_id = child_info_id;
}

public Byte getChild_order() {
	return child_order;
}

public void setChild_order(Byte child_order) {
	this.child_order = child_order;
}

public String getWork_item() {
	return work_item;
}

public void setWork_item(String work_item) {
	this.work_item = work_item;
}

public String getRemarks() {
	return remarks;
}

public void setRemarks(String remarks) {
	this.remarks = remarks;
}

}