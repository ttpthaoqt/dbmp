package jp.co.toshiba.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * UserInfo
 */
@Entity
@Table(name = "user_info")
public class UserInfo {

  // ------------------------
  // PRIVATE FIELDS
  // ------------------------
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int user_info_id;
  
  @NotNull
  private String user_id;
  
  @NotNull
  private String password;
  
  @NotNull
  private short authority;
  
  @NotNull
  private String server_id;
  
  @NotNull
  private String server_password;
  
  @NotNull
  private String user_name;
  
  @NotNull
  private short del_flag;

  // ------------------------
  // PUBLIC METHODS
  // ------------------------
  
  public int getUser_info_id() {
	return user_info_id;
  }
	
	public void setUser_info_id(int user_info_id) {
		this.user_info_id = user_info_id;
	}
	
	public String getUser_id() {
		return user_id;
	}
	
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public short getAuthority() {
		return authority;
	}
	
	public void setAuthority(short authority) {
		this.authority = authority;
	}
	
	public String getServer_id() {
		return server_id;
	}
	
	public void setServer_id(String server_id) {
		this.server_id = server_id;
	}
	
	public String getServer_password() {
		return server_password;
	}
	
	public void setServer_password(String server_password) {
		this.server_password = server_password;
	}
	
	public String getUser_name() {
		return user_name;
	}
	
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	
	public short getDel_flag() {
		return del_flag;
	}
	
	public void setDel_flag(short del_flag) {
		this.del_flag = del_flag;
	}

//	public UserInfo(long user_info_id) { 
//		this.user_info_id = user_info_id;
//	}
//
//	public UserInfo(String user_id, String password) {
//		this.user_id = user_id;
//		this.password = password;
//	}  
} // class User
