/**
 * 
 */
package jp.co.toshiba.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Parent Group
 */
@Entity
@Table(name = "parent_group")
public class ParentGroup {
	
	// ------------------------
	// PRIVATE FIELDS
	// ------------------------

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int parent_group_id;
	
	@NotNull
	private int group_info_id;
	
	@NotNull
	private int parent_id;
	
	@NotNull
	private short del_flag;
	
	// ------------------------
	// PUBLIC METHODS
	// ------------------------
	
	public int getParent_group_id() {
		return parent_group_id;
	}

	public void setParent_group_id(int parent_group_id) {
		this.parent_group_id = parent_group_id;
	}

	public int getGroup_info_id() {
		return group_info_id;
	}

	public void setGroup_info_id(int group_info_id) {
		this.group_info_id = group_info_id;
	}

	public int getParent_id() {
		return parent_id;
	}

	public void setParent_id(int parent_id) {
		this.parent_id = parent_id;
	}

	public short getDel_flag() {
		return del_flag;
	}

	public void setDel_flag(short del_flag) {
		this.del_flag = del_flag;
	}

}
