/**
 * 
 */
package jp.co.toshiba.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * ParameterInfo
 */
@Entity
@Table(name = "parameter_info")
public class ParameterInfo {

	// ------------------------
	// PRIVATE FIELDS
	// ------------------------

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int parameter_info_id;
	
	@NotNull
	private int group_info_id;
	
	@NotNull
	private String parameter_name;
	
	@NotNull
	private String parameter_value;
	
	@NotNull
	private short del_flag;
	
	// ------------------------
	// PUBLIC METHODS
	// ------------------------
	
	public int getParameter_info_id() {
		return parameter_info_id;
	}

	public void setParameter_info_id(int parameter_info_id) {
		this.parameter_info_id = parameter_info_id;
	}

	public int getGroup_info_id() {
		return group_info_id;
	}

	public void setGroup_info_id(int group_info_id) {
		this.group_info_id = group_info_id;
	}

	public String getParameter_name() {
		return parameter_name;
	}

	public void setParameter_name(String parameter_name) {
		this.parameter_name = parameter_name;
	}

	public String getParameter_value() {
		return parameter_value;
	}

	public void setParameter_value(String parameter_value) {
		this.parameter_value = parameter_value;
	}

	public short getDel_flag() {
		return del_flag;
	}

	public void setDel_flag(short del_flag) {
		this.del_flag = del_flag;
	}
}
