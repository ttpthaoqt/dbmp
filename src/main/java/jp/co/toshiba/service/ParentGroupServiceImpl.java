/**
 * 
 */
package jp.co.toshiba.service;

import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import jp.co.toshiba.entity.ParentGroup;
import jp.co.toshiba.repository.ParentGroupRepository;

/**
 * 
 *
 */

@Service
@Validated
public class ParentGroupServiceImpl implements ParentGroupService{
	
private ParentGroupRepository repository;
	
    @Autowired
    public void setParentGroupRepository(ParentGroupRepository ParentGroupReponsitory) {
        this.repository = ParentGroupReponsitory;
    }
    
    @Override
	public List<ParentGroup> getDetailByGroupID(int group_info_id){
    	try {
    		return repository.getDetailByGroupID(group_info_id);
    	} catch(NoResultException e) {
	        return null;
	    }
    }

}
