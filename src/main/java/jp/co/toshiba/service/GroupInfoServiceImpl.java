/**
 * 
 */
package jp.co.toshiba.service;

import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import jp.co.toshiba.entity.ExecutionPlan;
import jp.co.toshiba.entity.GroupInfo;
import jp.co.toshiba.entity.GroupInfoStatus;
import jp.co.toshiba.entity.UserInfo;
import jp.co.toshiba.repository.GroupInfoRepository;

/**
 * 
 *
 */

@Service
@Validated
public class GroupInfoServiceImpl implements GroupInfoService{
	
private GroupInfoRepository repository;
	
    @Autowired
    public void setGroupInfoRepository(GroupInfoRepository GroupInfoReponsitory) {
        this.repository = GroupInfoReponsitory;
    }
    
    @Override
	public List<GroupInfo> getDetail(int executionPlanID){
    	try {
    		return repository.getDetail(executionPlanID);
    	} catch(NoResultException e) {
	        return null;
	    }
    }
    
    @Override
	public GroupInfo getDetail(int groupID, int executionPlanID){
    	try {
    		return repository.getDetail(groupID, executionPlanID);
    	} catch(NoResultException e) {
	        return null;
	    }
    }
    
    @Override
    public List<GroupInfoStatus> getGroupListStatus(int execution_plan_id) {
    	try {
    		return repository.getGroupListStatus(execution_plan_id);
    	} catch (NoResultException e){
    		return null;
    	}
    }
    
    @Override
    public List<GroupInfo> getList() {
		// TODO Auto-generated method stub
    	try {
    		return repository.findAll();
    	} catch(NoResultException e) {
	        return null;
	    }
	}
    
    @Override
    public GroupInfo save(GroupInfo groupInfo) {
    	try {
	    	GroupInfo existing = repository.findOne(groupInfo.getGroup_info_id());
	    	if(existing == null) {
	    		return repository.saveAndFlush(groupInfo);
	    	} else {
	    		return null;
	    	}
    	} catch(NoResultException e) {
	        return null;
	    }
    }
    
    public GroupInfo update(GroupInfo groupInfo) {
    	try {
	    	GroupInfo existing = repository.findOne(groupInfo.getGroup_info_id());
	    	if(existing != null) {
	    		
	    	}
	    	return repository.saveAndFlush(groupInfo);
    	} catch(NoResultException e) {
	        return null;
	    }
    }

}
