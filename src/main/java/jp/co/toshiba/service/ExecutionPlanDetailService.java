/**
 * 
 */
package jp.co.toshiba.service;

import java.util.List;

import jp.co.toshiba.entity.ExecutionPlanDetail;

/**
 * @author Administrator
 *
 */
public interface ExecutionPlanDetailService {
	
	List<ExecutionPlanDetail> getDetail(int executionPlanID);
	List<ExecutionPlanDetail> getDetail(int groupID, int executionPlanID);
	ExecutionPlanDetail getOne(int execution_plan_detail_id);
	ExecutionPlanDetail update(ExecutionPlanDetail executionPlanDetail);

}
