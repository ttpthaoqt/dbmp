/**
 * 
 */
package jp.co.toshiba.service;


import jp.co.toshiba.entity.ChildInfo;

/**
 * @author Administrator
 *
 */
public interface ChildInfoService {
	ChildInfo getOne(int child_info_id);
	ChildInfo save(ChildInfo cInfo);
	ChildInfo update(ChildInfo cInfo);

}
