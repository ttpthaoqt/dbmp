/**
 * 
 */
package jp.co.toshiba.service;

import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import jp.co.toshiba.entity.ExecutionPlan;
import jp.co.toshiba.entity.ExecutionPlanDetail;
import jp.co.toshiba.repository.ExecutionPlanRepository;

/**
 * @author Administrator
 *
 */
@Service
@Validated
public class ExecutionPlanServiceImpl implements ExecutionPlanService {

	private ExecutionPlanRepository repository;
	
    @Autowired
    public void setExecutionPlanRepository(ExecutionPlanRepository executionPlanRepository) {
        this.repository = executionPlanRepository;
    }
    
    @Override
    public List<ExecutionPlan> getList() {
		// TODO Auto-generated method stub
    	try {
    		return repository.findAll();
    	} catch(NoResultException e) {
	        return null;
	    }
	}
    
    @Override
    public ExecutionPlan save(ExecutionPlan executionPlan) {
    	try {
	    	ExecutionPlan existing = repository.findOne(executionPlan.getExecution_plan_id());
	    	if(existing == null) {
	    		return repository.saveAndFlush(executionPlan);
	    	} else {
	    		return null;
	    	}
    	} catch(NoResultException e) {
	        return null;
	    }
    }
    
    public ExecutionPlan update(ExecutionPlan executionPlan) {
    	try {
    		try {
        		return repository.update(executionPlan);
        	} catch(NoResultException e) {
    	        return null;
    	    }
    	} catch(NoResultException e) {
	        return null;
	    }
    }
    
    @Override
	public ExecutionPlan getOne(int execution_plan_id){
    	return repository.findOne(execution_plan_id);
    }
    
    @Override
    public ExecutionPlan getExePlanTop(int pType) {
    	return repository.getExePlanTop(pType);
    }
    
    public List<ExecutionPlan> getListExePlanSearch(int pType){
    	return repository.getListExePlanSearch(pType);
    }
    
    public List<ExecutionPlan> getListExePlanSearch(int pType, Date start_time, Date end_time, String implemented_name ){
    	return repository.getListExePlanSearch(pType, start_time, end_time, implemented_name);
    }
}
