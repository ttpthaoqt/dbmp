package jp.co.toshiba.service;

import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import jp.co.toshiba.entity.UserInfo;
import jp.co.toshiba.repository.UserRepository;


@Service
@Validated
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepository;
    
	@Override
	public UserInfo getUserId(String user_id) {
		// TODO Auto-generated method stub
		return userRepository.findOne(1);
	}
	
	@Override
	public UserInfo checkUserValid(String user_id, String password) {
		return userRepository.checkUserValid(user_id, password);
	}	
	
	@Override
    public List<UserInfo> getList() {
		// TODO Auto-generated method stub
		try {
			return userRepository.findAll();
		} catch(NoResultException e) {
	        return null;
	    }
	}


}
