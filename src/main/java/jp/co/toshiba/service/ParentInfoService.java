/**
 * 
 */
package jp.co.toshiba.service;

import java.util.List;

import jp.co.toshiba.entity.ParentInfo;
import jp.co.toshiba.entity.VewParentChildInf;

/**
 * @author Administrator
 *
 */
public interface ParentInfoService {
	List<ParentInfo> getList();
	ParentInfo getOne(int parent_info_id);
	List<VewParentChildInf> getViewList();
	List<VewParentChildInf> getSearchViewList(int selectedParentInfo, String workItem);
	ParentInfo save(ParentInfo pInfo);
	ParentInfo update(ParentInfo pInfo);
}
