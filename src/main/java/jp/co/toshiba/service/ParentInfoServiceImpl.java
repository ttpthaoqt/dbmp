/**
 * 
 */
package jp.co.toshiba.service;

import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import jp.co.toshiba.entity.ChildInfo;
import jp.co.toshiba.entity.ParentInfo;
import jp.co.toshiba.entity.VewParentChildInf;
import jp.co.toshiba.repository.ParentInfoRepository;

/**
 * 
 *
 */

@Service
@Validated
public class ParentInfoServiceImpl implements ParentInfoService{
	
	@Autowired
	private ParentInfoRepository parentInfoRepository;	
	
	@Override
    public List<ParentInfo> getList() {
		try {
			return parentInfoRepository.getList();
		} catch(NoResultException e) {
	        return null;
	    }
	}
	
	@Override
	public ParentInfo getOne(int parent_info_id){
    	return parentInfoRepository.findOne(parent_info_id);
    }

	@Override
	public List<VewParentChildInf> getViewList() {
		// TODO Auto-generated method stub
		try {
			return parentInfoRepository.getViewList();
		} catch(NoResultException e) {
	        return null;
	    }
	}

	@Override		
	public ParentInfo save(ParentInfo pInfo) {
		try {
			pInfo = parentInfoRepository.insert(pInfo);
			return pInfo;
		}catch(Exception e) {
		}
		return null;
	}
	
	@Override		
	public ParentInfo update(ParentInfo pInfo) {
		try {
			pInfo = parentInfoRepository.update(pInfo);
			return pInfo;
		}catch(Exception e) {
		}
		return null;
	}

	@Override
	public List<VewParentChildInf> getSearchViewList(int selectedParentInfo, String workItem) {
		try {
			return parentInfoRepository.getSearchViewList(selectedParentInfo, workItem);
		} catch(NoResultException e) {
	        return null;
	    }
	}

}
