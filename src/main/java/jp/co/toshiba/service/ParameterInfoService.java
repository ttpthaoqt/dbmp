/**
 * 
 */
package jp.co.toshiba.service;

import java.util.List;

import jp.co.toshiba.entity.ParameterInfo;

/**
 * @author Administrator
 *
 */
public interface ParameterInfoService {
	
	List<ParameterInfo> getDetailByGroupID(int group_info_id);

}
