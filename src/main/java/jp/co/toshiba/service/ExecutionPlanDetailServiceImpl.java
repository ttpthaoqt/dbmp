/**
 * 
 */
package jp.co.toshiba.service;

import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import jp.co.toshiba.entity.ExecutionPlan;
import jp.co.toshiba.entity.ExecutionPlanDetail;
import jp.co.toshiba.repository.ExecutionPlanDetailRepository;

/**
 * 
 *
 */

@Service
@Validated
public class ExecutionPlanDetailServiceImpl implements ExecutionPlanDetailService{
	
private ExecutionPlanDetailRepository repository;
	
    @Autowired
    public void setExecutionPlanDetailRepository(ExecutionPlanDetailRepository executionPlanDetailReponsitory) {
        this.repository = executionPlanDetailReponsitory;
    }
    
    @Override
	public List<ExecutionPlanDetail> getDetail(int executionPlanID){
    	try {
    		return repository.getDetail(executionPlanID);
    	} catch(NoResultException e) {
	        return null;
	    }
    }
    
    @Override
	public List<ExecutionPlanDetail> getDetail(int groupID, int executionPlanID){
    	try {
    		return repository.getDetail(groupID, executionPlanID);
    	} catch(NoResultException e) {
	        return null;
	    }
    }
    
    @Override
	public ExecutionPlanDetail getOne(int execution_plan_detail_id){
    	return repository.findOne(execution_plan_detail_id);
    }
    
    @Override
    public ExecutionPlanDetail update(ExecutionPlanDetail executionPlanDetail) {
    	try {
    		return repository.update(executionPlanDetail);
    	} catch(NoResultException e) {
	        return null;
	    }
    }

}
