/**
 * 
 */
package jp.co.toshiba.service;

import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import jp.co.toshiba.entity.ParameterInfo;
import jp.co.toshiba.repository.ParameterInfoRepository;

/**
 * 
 *
 */

@Service
@Validated
public class ParameterInfoServiceImpl implements ParameterInfoService{
	
private ParameterInfoRepository repository;
	
    @Autowired
    public void setParameterInfoRepository(ParameterInfoRepository ParameterInfoReponsitory) {
        this.repository = ParameterInfoReponsitory;
    }
    
    @Override
	public List<ParameterInfo> getDetailByGroupID(int group_info_id){
    	try {
    		return repository.getDetailByGroupID(group_info_id);
    	} catch(NoResultException e) {
	        return null;
	    }
    }

}
