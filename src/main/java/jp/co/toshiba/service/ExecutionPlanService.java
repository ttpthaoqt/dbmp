/**
 * 
 */
package jp.co.toshiba.service;

import java.util.Date;
import java.util.List;

import jp.co.toshiba.entity.ExecutionPlan;
import jp.co.toshiba.entity.ExecutionPlanDetail;

/**
 * @author Administrator
 *
 */
public interface ExecutionPlanService {
	ExecutionPlan save(ExecutionPlan executionPlan);
	List<ExecutionPlan> getList();
	ExecutionPlan getOne(int execution_plan_id);
	ExecutionPlan getExePlanTop(int pType);
	List<ExecutionPlan> getListExePlanSearch(int pType);
	List<ExecutionPlan> getListExePlanSearch(int pType, Date start_time, Date end_time, String implemented_name );
	ExecutionPlan update(ExecutionPlan executionPlan);
}
