/**
 * 
 */
package jp.co.toshiba.service;

import java.util.List;

import jp.co.toshiba.entity.GroupInfo;
import jp.co.toshiba.entity.GroupInfoStatus;

/**
 * @author Administrator
 *
 */
public interface GroupInfoService {
	
	List<GroupInfo> getDetail(int executionPlanID);
	GroupInfo getDetail(int groupID, int executionPlanID);
	List<GroupInfoStatus> getGroupListStatus(int execution_plan_id);
	GroupInfo save(GroupInfo groupInfo);
	List<GroupInfo> getList();

}
