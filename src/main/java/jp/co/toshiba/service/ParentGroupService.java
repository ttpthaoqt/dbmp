/**
 * 
 */
package jp.co.toshiba.service;

import java.util.List;

import jp.co.toshiba.entity.ParentGroup;

/**
 * @author Administrator
 *
 */
public interface ParentGroupService {
	
	List<ParentGroup> getDetailByGroupID(int group_info_id);

}
