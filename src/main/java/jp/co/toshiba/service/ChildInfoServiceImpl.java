/**
 * 
 */
package jp.co.toshiba.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import jp.co.toshiba.entity.ChildInfo;
import jp.co.toshiba.entity.ExecutionPlanDetail;
import jp.co.toshiba.repository.ChildInfoRepository;

/**
 * 
 *
 */

@Service
@Validated
public class ChildInfoServiceImpl implements ChildInfoService{
	
	@Autowired
	private ChildInfoRepository repository;
	
	@Override
	public ChildInfo getOne(int child_info_id){
    	return repository.findOne(child_info_id);
    }

	@Override
	public ChildInfo save(ChildInfo cInfo) {
		try {
			return repository.insert(cInfo);
		}catch(Exception e) {
		}
		return null;
	}

	@Override
	public ChildInfo update(ChildInfo cInfo) {
		try {
			return repository.update(cInfo);
		}catch(Exception e) {
		}
		return null;
	}

}
