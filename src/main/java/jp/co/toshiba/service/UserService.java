package jp.co.toshiba.service;

import java.util.List;

import jp.co.toshiba.entity.UserInfo;

public interface UserService {

//    User save(User user);
//
//    List<User> getList();
	UserInfo getUserId(String user_id);
	UserInfo checkUserValid(String user_id, String password);
	List<UserInfo> getList();

}
