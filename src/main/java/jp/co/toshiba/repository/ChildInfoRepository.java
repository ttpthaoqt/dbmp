/**
 * 
 */
package jp.co.toshiba.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import jp.co.toshiba.entity.ChildInfo;

/**
 * @author Administrator
 *
 */
public interface ChildInfoRepository extends JpaRepository<ChildInfo, Integer>, ChildInfoRepositoryCustom {

}
