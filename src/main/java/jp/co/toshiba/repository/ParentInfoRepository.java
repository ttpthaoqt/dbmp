/**
 * 
 */
package jp.co.toshiba.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import jp.co.toshiba.entity.ParentInfo;
import jp.co.toshiba.entity.VewParentChildInf;

/**
 * @author Administrator
 *
 */
public interface ParentInfoRepository extends JpaRepository<ParentInfo, Integer>, ParentInfoRepositoryCustom {	

}
