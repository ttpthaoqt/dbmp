/**
 * 
 */
package jp.co.toshiba.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import jp.co.toshiba.entity.ParentGroup;

/**
 * @author Administrator
 *
 */
public interface ParentGroupRepository extends JpaRepository<ParentGroup, Integer>, ParentGroupRepositoryCustom {

}
