/**
 * 
 */
package jp.co.toshiba.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jp.co.toshiba.entity.ExecutionPlanDetail;

/**
 * @author Administrator
 *
 */
@Repository
@Transactional(readOnly = true)
public class ExecutionPlanDetailRepositoryImpl implements ExecutionPlanDetailRepositoryCustom {
	
	@PersistenceContext
    EntityManager entityManager;
	
	@Override
	public List<ExecutionPlanDetail> getDetail(int execution_plan_id) {
		String strQuery = "SELECT * FROM execution_plan_detail WHERE del_flag = 0 AND execution_plan_id = :execution_plan_id";
		try {
			Query query = entityManager.createNativeQuery(strQuery, ExecutionPlanDetail.class);
			query.setParameter("execution_plan_id", execution_plan_id);
			List<ExecutionPlanDetail> result = query.getResultList();
			return result;
		} catch(NoResultException e) {
	        return null;
	    }

	}
	
	@Override
	public List<ExecutionPlanDetail> getDetail(int group_info_id, int execution_plan_id) {
		String strQuery = "SELECT * FROM execution_plan_detail WHERE del_flag = 0 AND group_info_id = :group_info_id AND execution_plan_id = :execution_plan_id";
		try {
			Query query = entityManager.createNativeQuery(strQuery, ExecutionPlanDetail.class);
			query.setParameter("execution_plan_id", execution_plan_id);
			query.setParameter("group_info_id", group_info_id);
			List<ExecutionPlanDetail> result = query.getResultList();
			return result;
		} catch(NoResultException e) {
	        return null;
	    }
	}
	
	@Override 
	@Transactional
	public ExecutionPlanDetail update(ExecutionPlanDetail executionPlanDetail) throws javax.persistence.EntityExistsException{
		try {
			entityManager.merge(executionPlanDetail);
			return executionPlanDetail;
		} catch(NoResultException e) {
	        return null;
	    }
	}

}
