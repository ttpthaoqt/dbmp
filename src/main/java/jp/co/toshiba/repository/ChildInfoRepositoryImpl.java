/**
 * 
 */
package jp.co.toshiba.repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jp.co.toshiba.entity.ChildInfo;
import jp.co.toshiba.entity.ExecutionPlanDetail;
import jp.co.toshiba.entity.ParentInfo;

/**
 * @author Administrator
 *
 */
@Repository
@Transactional(readOnly = true)
public class ChildInfoRepositoryImpl implements ChildInfoRepositoryCustom {
	
	@PersistenceContext
    EntityManager entityManager;

	@Override
	@Transactional
	public ChildInfo insert(ChildInfo cInfo) {
		try {
			entityManager.persist(cInfo);
			return cInfo;
		} catch(NoResultException e) {
	        return null;
	    }
	}
	
	@Override 
	@Transactional
	public ChildInfo update(ChildInfo cInfo) throws javax.persistence.EntityExistsException{
		try {
			entityManager.merge(cInfo);
			return cInfo;
		} catch(NoResultException e) {
	        return null;
	    }
	}

}
