/**
 * 
 */
package jp.co.toshiba.repository;

import java.util.Date;
import java.util.List;

import jp.co.toshiba.entity.ExecutionPlan;

/**
 * 
 *
 */
public interface ExecutionPlanRepositoryCustom {
	
	ExecutionPlan getExePlanID(int execution_plan_id);
	ExecutionPlan getExePlanTop(int pType);
	List<ExecutionPlan> getListExePlanSearch(int pType);
	List<ExecutionPlan> getListExePlanSearch(int pType, Date start_time, Date end_time, String implemented_name );
	ExecutionPlan update(ExecutionPlan executionPlan);
}
