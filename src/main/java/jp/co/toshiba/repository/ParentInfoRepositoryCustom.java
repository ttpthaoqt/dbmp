/**
 * 
 */
package jp.co.toshiba.repository;

import java.util.List;

import jp.co.toshiba.entity.ParentInfo;
import jp.co.toshiba.entity.VewParentChildInf;

/**
 * 
 *
 */
public interface ParentInfoRepositoryCustom {
	List<ParentInfo> getList();
	List<VewParentChildInf> getViewList();
	List<VewParentChildInf> getSearchViewList(int selectedParentInfo, String workItem);
//	ParentInfo insert(int parent_no, String parent_name);
	ParentInfo insert(ParentInfo pInfo);
	ParentInfo update(ParentInfo pInfo);
}
