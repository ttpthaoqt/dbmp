/**
 * 
 */
package jp.co.toshiba.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jp.co.toshiba.entity.ChildInfo;
import jp.co.toshiba.entity.ParentInfo;
import jp.co.toshiba.entity.UserInfo;
import jp.co.toshiba.entity.VewParentChildInf;

/**
 * @author Administrator
 *
 */
@Repository
@Transactional(readOnly = true)
public class ParentInfoRepositoryImpl implements ParentInfoRepositoryCustom {
	
	@PersistenceContext
    EntityManager entityManager;
	
	private static String SQL_GET_SEARCH_VIEW_LIST = "SELECT a.* FROM parentInfo_childInfo a"; 

	@Override
	public List<ParentInfo> getList() {
		try {
	    	Query query = entityManager.createNativeQuery("SELECT a.* FROM parent_info a WHERE a.del_flag = 0", ParentInfo.class);
			return query.getResultList();
		} catch(NoResultException e) {
	        return null;
	    }
	}

	@Override
	public List<VewParentChildInf> getViewList() {
		try {
			Query query = entityManager.createNativeQuery(SQL_GET_SEARCH_VIEW_LIST);
			List<Object[]> rows = query.getResultList();
			List<VewParentChildInf> result = new ArrayList<VewParentChildInf>(rows.size());
			for (Object[] row : rows) {
			    result.add(new VewParentChildInf(
			    		(Integer) row[0],
			    		(Integer) row[1],
			    		(String) row[2],
			    		(Integer) row[3],
			    		(Byte) row[4],
			    		(String) row[5],
			    		(String) row[6]));
			}

			return result;
		} catch(NoResultException e) {
	        return null;
	    }
	}
	
	@Override
	@Transactional
	public ParentInfo insert(ParentInfo pInfo) {
		try {
			entityManager.persist(pInfo);
			return pInfo;
		} catch(NoResultException e) {
	        return null;
	    }
	}
	
	@Override 
	@Transactional
	public ParentInfo update(ParentInfo pInfo) throws javax.persistence.EntityExistsException{
		try {
			entityManager.merge(pInfo);
			return pInfo;
		} catch(NoResultException e) {
	        return null;
	    }
	}

	@Override
	public List<VewParentChildInf> getSearchViewList(int selectedParentInfo, String workItem) {
		try {
			String sql = SQL_GET_SEARCH_VIEW_LIST + " WHERE 1=1";
			if(selectedParentInfo != -1) {
				sql += " AND a.parent_info_id = :selectedParentInfo";
			}
			if (!workItem.equals(String.valueOf(""))) {
				sql += " AND (a.work_item LIKE :workItem)";
			}
			Query query = entityManager.createNativeQuery(sql);
			if(selectedParentInfo != -1) {
				query.setParameter("selectedParentInfo", selectedParentInfo);
			}
			if (!workItem.equals(String.valueOf(""))) {
				query.setParameter("workItem", "%"+ workItem + "%");
			}
			List<Object[]> rows = query.getResultList();
			List<VewParentChildInf> result = new ArrayList<VewParentChildInf>(rows.size());
			for (Object[] row : rows) {
			    result.add(new VewParentChildInf(
			    		(Integer) row[0],
			    		(Integer) row[1],
			    		(String) row[2],
			    		(Integer) row[3],
			    		(Byte) row[4],
			    		(String) row[5],
			    		(String) row[6]));
			}
			return result;
		} catch(NoResultException e) {
	        return null;
	    }
	}
	
	

}
