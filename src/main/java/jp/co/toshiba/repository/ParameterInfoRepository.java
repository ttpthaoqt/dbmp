/**
 * 
 */
package jp.co.toshiba.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import jp.co.toshiba.entity.ParameterInfo;

/**
 * @author Administrator
 *
 */
public interface ParameterInfoRepository extends JpaRepository<ParameterInfo, Integer>, ParameterInfoRepositoryCustom {

}
