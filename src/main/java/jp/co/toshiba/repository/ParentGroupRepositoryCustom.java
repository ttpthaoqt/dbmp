/**
 * 
 */
package jp.co.toshiba.repository;

import java.util.List;

import jp.co.toshiba.entity.ParentGroup;

/**
 * 
 *
 */
public interface ParentGroupRepositoryCustom {

	List<ParentGroup> getDetailByGroupID(int group_info_id);
	
}
