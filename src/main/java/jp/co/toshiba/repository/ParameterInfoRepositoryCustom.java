/**
 * 
 */
package jp.co.toshiba.repository;

import java.util.List;

import jp.co.toshiba.entity.ParameterInfo;

/**
 * 
 *
 */
public interface ParameterInfoRepositoryCustom {

	List<ParameterInfo> getDetailByGroupID(int group_info_id);
	
}
