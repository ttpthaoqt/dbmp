/**
 * 
 */
package jp.co.toshiba.repository;

import java.util.List;

import jp.co.toshiba.entity.ExecutionPlanDetail;

/**
 * 
 *
 */
public interface ExecutionPlanDetailRepositoryCustom {

	List<ExecutionPlanDetail> getDetail(int executionPlanID);
	List<ExecutionPlanDetail> getDetail(int groupID, int executionPlanID);
	ExecutionPlanDetail update(ExecutionPlanDetail executionPlanDetail);
	
}
