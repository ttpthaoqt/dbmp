package jp.co.toshiba.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jp.co.toshiba.entity.UserInfo;

@Repository
@Transactional(readOnly = true)
public class UserRepositoryImpl implements UserRepositoryCustom {
	
    @PersistenceContext
    EntityManager entityManager;
    
    @Override
    public UserInfo checkUserValid(String user_id, String password) {
    	try {
	    	Query query = entityManager.createNativeQuery("SELECT a.* FROM user_info a WHERE a.user_id = :user_id and a.password = :password and a.del_flag = 0", UserInfo.class);
	        query.setParameter("user_id", user_id);
	        query.setParameter("password", password);
	        List<UserInfo> lstUserInfo = query.getResultList();
	        if(lstUserInfo.size() > 0) {
	        	return lstUserInfo.get(0);    	
	        }else {
	        	return null;
	        }
    	} catch(NoResultException e) {
	        return null;
	    }
    }
}
