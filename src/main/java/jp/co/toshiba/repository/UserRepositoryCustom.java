package jp.co.toshiba.repository;

import jp.co.toshiba.entity.UserInfo;

public interface UserRepositoryCustom {
	UserInfo checkUserValid(String user_id, String password);
}
