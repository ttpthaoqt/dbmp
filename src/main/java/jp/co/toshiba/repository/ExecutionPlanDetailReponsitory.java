/**
 * 
 */
package jp.co.toshiba.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import jp.co.toshiba.entity.ExecutionPlanDetail;

/**
 * @author Administrator
 *
 */
public interface ExecutionPlanDetailReponsitory extends JpaRepository<ExecutionPlanDetail, Integer> {

}
