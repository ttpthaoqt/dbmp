/**
 * 
 */
package jp.co.toshiba.repository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jp.co.toshiba.entity.ExecutionPlan;
import jp.co.toshiba.entity.ExecutionPlanDetail;
import jp.co.toshiba.entity.VewParentChildInf;

/**
 * 
 *
 */
@Repository
@Transactional(readOnly = true)
public class ExecutionPlanRepositoryImpl implements ExecutionPlanRepositoryCustom {
	
	@PersistenceContext
    EntityManager entityManager;
	
	@Override
	public ExecutionPlan getExePlanID(int execution_plan_id) {
		try {
			ExecutionPlan result = entityManager.find(ExecutionPlan.class, execution_plan_id);
			return result;
		} catch(NoResultException e) {
	        return null;
	    }
	}
	
	/**
	 * pType: 1 - 実行権限; 2 - 参照権限 
	 * **/
	@Override
	public List<ExecutionPlan> getListExePlanSearch(int pType) {
		String strQuery = "SELECT * FROM execution_plan ";
		strQuery += " WHERE del_flag = 0";
		if (pType==1) {
			strQuery += " AND status != 2 ";
		}
		strQuery += " ORDER BY execution_plan_id";
		try {
			Query query = entityManager.createNativeQuery(strQuery, ExecutionPlan.class);
			List<ExecutionPlan> result = query.getResultList();
			return result;
		} catch(NoResultException e) {
	        return null;
	    }
	}
	
	/**
	 * pType: 1 - 実行権限; 2 - 参照権限 
	 * **/
	@Override
	public List<ExecutionPlan> getListExePlanSearch(int pType, Date start_time, Date end_time, String implemented_name ) {
		String searchStartDate = "";
		String searchEndDate = "";
		String searchImplementedName = "";
		String strStartTime = "";
		String strEndTime = "";
		
		DateFormat df = new SimpleDateFormat("yyyy-mm-dd");   
		if (start_time!=null) {
			strStartTime = df.format(start_time);
			searchStartDate = " AND DATE_FORMAT(plan_date, '%Y-%m-%d') >= :start_time ";
		}
		
		if (end_time!=null) {
			strEndTime = df.format(end_time);
			searchEndDate = " AND DATE_FORMAT(plan_date, '%Y-%m-%d') <= :end_time ";
		}

		if (implemented_name!=null && !implemented_name.equals(String.valueOf(""))) {
			searchImplementedName = " AND implemented_name LIKE :implemented_name ";
		}
		
		String strQuery = "SELECT * FROM execution_plan ";
		strQuery +=  " WHERE del_flag = 0";
		if (pType==1) {
			strQuery += " AND status != 2 ";
		}
		strQuery += searchStartDate;
		strQuery += searchEndDate;
		strQuery += searchImplementedName;
		strQuery += " ORDER BY execution_plan_id";
		try {
			Query query = entityManager.createNativeQuery(strQuery, ExecutionPlan.class);
			if (start_time!=null) {
				query.setParameter("start_time", strStartTime);
			}
			if (end_time!=null) {
				query.setParameter("end_time", strEndTime);
			}
			if (implemented_name!=null && !implemented_name.equals(String.valueOf(""))) {
				query.setParameter("implemented_name", "%" + implemented_name + "%");
			}
			List<ExecutionPlan> result = query.getResultList();
			return result;
		} catch(NoResultException e) {
	        return null;
	    }
	}

	/**
	 * pType: 1 - 実行権限; 2 - 参照権限 
	 * **/
	@Override
	public ExecutionPlan getExePlanTop(int pType) {
		String strQuery = "SELECT * FROM execution_plan WHERE del_flag = 0";
		if (pType==1) {
			strQuery += " AND status != 2";
		}
		strQuery += " ORDER BY execution_plan_id LIMIT 0, 1";
		try {
			Query query = entityManager.createNativeQuery(strQuery, ExecutionPlan.class);
			ExecutionPlan result = (ExecutionPlan) query.getSingleResult();
			return result;
		} catch(NoResultException e) {
	        return null;
	    }
	}
	
	@Override 
	@Transactional
	public ExecutionPlan update(ExecutionPlan executionPlan) throws javax.persistence.EntityExistsException{
		try {
			entityManager.merge(executionPlan);
			return executionPlan;
		} catch(NoResultException e) {
	        return null;
	    }
	}

}
