/**
 * 
 */
package jp.co.toshiba.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import jp.co.toshiba.entity.ExecutionPlan;

/**
 * @author Administrator
 *
 */
public interface ExecutionPlanRepository extends JpaRepository<ExecutionPlan, Integer>, ExecutionPlanRepositoryCustom {

}
