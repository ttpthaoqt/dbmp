/**
 * 
 */
package jp.co.toshiba.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import jp.co.toshiba.entity.GroupInfo;

/**
 * @author Administrator
 *
 */
public interface GroupInfoRepository extends JpaRepository<GroupInfo, Integer>, GroupInfoRepositoryCustom {

}
