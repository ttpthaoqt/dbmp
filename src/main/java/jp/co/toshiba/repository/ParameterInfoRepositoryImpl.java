/**
 * 
 */
package jp.co.toshiba.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jp.co.toshiba.entity.ParameterInfo;

/**
 * @author Administrator
 *
 */
@Repository
@Transactional(readOnly = true)
public class ParameterInfoRepositoryImpl implements ParameterInfoRepositoryCustom {
	
	@PersistenceContext
    EntityManager entityManager;
	
	@Override
	public List<ParameterInfo> getDetailByGroupID(int group_info_id) {
		String strQuery = "SELECT * FROM parameter_info WHERE del_flag = 0 AND group_info_id = :group_info_id";
		try {
			Query query = entityManager.createNativeQuery(strQuery, ParameterInfo.class);
			query.setParameter("group_info_id", group_info_id);
			List<ParameterInfo> result = query.getResultList();
			return result;
		} catch(NoResultException e) {
	        return null;
	    }
	}

}
