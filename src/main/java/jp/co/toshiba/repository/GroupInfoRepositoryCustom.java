/**
 * 
 */
package jp.co.toshiba.repository;

import java.util.List;

import jp.co.toshiba.entity.GroupInfo;
import jp.co.toshiba.entity.GroupInfoStatus;

/**
 * 
 *
 */
public interface GroupInfoRepositoryCustom {

	List<GroupInfo> getDetail(int executionPlanID);
	List<GroupInfoStatus> getGroupListStatus(int executionPlanID);
	GroupInfo getDetail(int group_info_id, int execution_plan_id);
	
}
