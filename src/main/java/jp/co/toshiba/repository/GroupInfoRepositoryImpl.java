/**
 * 
 */
package jp.co.toshiba.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jp.co.toshiba.entity.GroupInfo;
import jp.co.toshiba.entity.GroupInfoStatus;

/**
 * @author Administrator
 *
 */
@Repository
@Transactional(readOnly = true)
public class GroupInfoRepositoryImpl implements GroupInfoRepositoryCustom {
	
	@PersistenceContext
    EntityManager entityManager;
	
	@Override
	public List<GroupInfo> getDetail(int execution_plan_id) {
		String strQuery = "SELECT * FROM group_info WHERE del_flag = 0 AND execution_plan_id = :execution_plan_id "
				+ "ORDER BY group_info_id";
		try {			
			Query query = entityManager.createNativeQuery(strQuery, GroupInfo.class);
			query.setParameter("execution_plan_id", execution_plan_id);
			List<GroupInfo> result = query.getResultList();
			return result;
		} catch(NoResultException e) {
	        return null;
	    }
	}
	
	@Override
	public GroupInfo getDetail(int group_info_id, int execution_plan_id) {
		String strQuery = "SELECT * FROM group_info WHERE del_flag = 0 AND group_info_id = :group_info_id AND execution_plan_id = :execution_plan_id "
				+ "ORDER BY group_info_id LIMIT 0, 1";
		try {
			Query query = entityManager.createNativeQuery(strQuery, GroupInfo.class);
			query.setParameter("group_info_id", group_info_id);
			query.setParameter("execution_plan_id", execution_plan_id);
			GroupInfo result = (GroupInfo) query.getSingleResult();
			return result;
		} catch(NoResultException e) {
	        return null;
	    }
	}
	
	@Override
	public List<GroupInfoStatus> getGroupListStatus(int execution_plan_id) {
		String strQuery = "SELECT * FROM viewgroupstatus WHERE del_flag = 0 AND execution_plan_id = :execution_plan_id";
		try {			
			Query query = entityManager.createNativeQuery(strQuery, GroupInfoStatus.class);
			query.setParameter("execution_plan_id", execution_plan_id);
			List<GroupInfoStatus> result = query.getResultList();
			return result;
		} catch(NoResultException e) {
	        return null;
	    }
	}
	
	

}
