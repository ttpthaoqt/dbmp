/**
 * 
 */
package jp.co.toshiba.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import jp.co.toshiba.entity.ExecutionPlan;

/**
 * @author Administrator
 *
 */
public interface ExecutionPlanReponsitory extends JpaRepository<ExecutionPlan, Integer> {
}
