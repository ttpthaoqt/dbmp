/**
 * 
 */
package jp.co.toshiba.repository;

import jp.co.toshiba.entity.ChildInfo;

/**
 * 
 *
 */
public interface ChildInfoRepositoryCustom {

	ChildInfo insert(ChildInfo cInfo);
	ChildInfo update(ChildInfo cInfo);

}
