package jp.co.toshiba.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import jp.co.toshiba.entity.UserInfo;

public interface UserRepository extends JpaRepository<UserInfo, Integer>, UserRepositoryCustom {
}
