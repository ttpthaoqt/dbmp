<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>親手順選択</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="scripts/jquery-1.11.3.min.js"></script>
  <script src="scripts/jquery-ui.min.js"></script>
  <link rel="stylesheet" href="css/jquery-ui.css">
  <link rel="stylesheet" href="css/tablescroll.css">
  <link rel="stylesheet" href="css/style.css">
  <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
  <section class="container" style="width: 600px; height: 575px">
    <h1>親手順選択</h1>
    <div class="content" style="padding-top: 30px;">
      <div>
        <table border="0" cellspacing="0" width="95%">
          <tbody>
            <tr>
              <td style="float: right;"><input type="button" name="btnSelection" value="選択"></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div id="tableContainer" class="tableContainer" style="margin-top: 20px; ">
          <table class="list_procedure table_scroll" width="100%" border="0">
          <thead>
            <tr class="title_head">
              <td style="width: 40px;"></td>
              <td style="width: 70px">順序</td>
              <td class="border_right" style="width: 399px;">親手順名称</td>
            </tr>
          </thead>
          <tbody style="height: 376px;">
            <tr>
              <td class="border_top_empty center" style="width: 40px;"><input type="checkbox" name=""></td>
              <td class="border_top_empty number" style="width: 70px;">1</td>
              <td class="border_right border_top_empty" style="width: 399px;">メンテナンスサーバ1</td>
            </tr>
            <tr>
              <td class="center"><input type="checkbox" name=""></td>
              <td class="number">2</td>
              <td class="border_right">メンテナンスサーバ1</td>
            </tr>
            <tr>
              <td class="center"><input type="checkbox" name=""></td>
              <td class="number">2</td>
              <td class="border_right">メンテナンスサーバ1</td>
            </tr>
            <tr>
              <td class="center"><input type="checkbox" name=""></td>
              <td class="number">2</td>
              <td class="border_right">メンテナンスサーバ1</td>
            </tr>
            <tr>
              <td class="center"><input type="checkbox" name=""></td>
              <td class="number">2</td>
              <td class="border_right">メンテナンスサーバ1</td>
            </tr>
            <tr>
              <td class="center"><input type="checkbox" name=""></td>
              <td class="number">2</td>
              <td class="border_right">メンテナンスサーバ1</td>
            </tr>
            <tr>
              <td class="center"><input type="checkbox" name=""></td>
              <td class="number">2</td>
              <td class="border_right">メンテナンスサーバ1</td>
            </tr>
            <tr>
              <td class="center"><input type="checkbox" name=""></td>
              <td class="number">2</td>
              <td class="border_right">メンテナンスサーバ1</td>
            </tr>
            <tr>
              <td class="center"><input type="checkbox" name=""></td>
              <td class="number">2</td>
              <td class="border_right">メンテナンスサーバ1</td>
            </tr>
            <tr>
              <td class="center"><input type="checkbox" name=""></td>
              <td class="number">2</td>
              <td class="border_right">メンテナンスサーバ1</td>
            </tr>
            <tr>
              <td class="center"><input type="checkbox" name=""></td>
              <td class="number">2</td>
              <td class="border_right">メンテナンスサーバ1</td>
            </tr>
          </tbody>
        </table>
        </div>
    </div>
  </section>
</body>
</html>
