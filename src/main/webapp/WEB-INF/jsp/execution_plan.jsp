<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>DB構築手順管理システム　【実行計画登録】</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="scripts/jquery-1.11.3.min.js"></script>
  <script src="scripts/jquery-ui.min.js"></script>
  <link rel="stylesheet" href="css/jquery-ui.css">
  <link rel="stylesheet" href="css/tablescroll.css">
  <link rel="stylesheet" href="css/style.css">
  <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <script type="text/javascript">
  $( function() {
      $( "#datepicker" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        buttonText: "Select date"
      });
      $(document).on('click', '#btnParentNoSelection', function (e) {
        e.preventDefault();
          var page = "${pageContext.request.contextPath}/parentno_selection";
          var $dialog = $('<div></div>')
          .html('<iframe src="' + page + '" width="100%" height="600"></iframe>')
          .dialog({
            resizable: false,
            height: 672,
            width: 700,
            modal: true,
            draggable: false,
            open: function(event,ui) {
                $(this).parent().focus();
            }
        });
      });
      $(document).on('click', '#btnExecutionPlanSelection', function (e) {
          e.preventDefault();
          var page = "${pageContext.request.contextPath}/execution_plan_selection?ptype=1&gid=1&epid=1&e=1";
          var $dialog = $('<div></div>')
          .html('<iframe src="' + page + '" width="100%" height="600"></iframe>')
          .dialog({
              resizable: false,
              height: 672,
              width: 800,
              modal: true,
              draggable: false,
              open: function(event,ui) {
                  $(this).parent().focus();
              }
          });
      });
    } );
  </script>
</head>
<body>
  <section class="container">
  	<c:if test="${empty sessionScope.user_id}">
		<c:redirect url="/"></c:redirect>
	</c:if>
    <div class="navi_bar">
      <div class="title_bar" style="width: 70%;"><h1>DB構築手順管理システム　【実行計画登録】</h1></div>
      <div class="title_bar" style="width: 120px;"><a href="${pageContext.request.contextPath}/top_menu">メニュー</a></div>
      <div class="title_bar">user:<c:out value="${sessionScope.user_name}"></c:out></div>
      <div class="title_bar"><a href="${pageContext.request.contextPath}/">Logout</a></div>
    </div>
    <div class="content">
	    <div>
	      <div class="display_div title_head label_left" style="width: 105px">実施予定日時</div>
	      <div class="display_div" style="width: 230px; height: 26px">
	        <input class="edit_text" id="datepicker" type="text" name="">
	      </div>
	      <div class="display_div" style="width: 30px;"></div>
	      <div class="display_div title_head label_left" style="width: 105px;">実施件名</div>
	      <div class="display_div"><input class="edit_text" style="width: 250px; margin-right: 10px;" type="text" name=""></div>
	      <div class="display_div"><input type="button" name="" value="保存"></div>
	      <div class="display_div" style="padding-right: 20px;"><input id="btnExecutionPlanSelection" type="button" name="" value="計画選択"></div>
	    </div>

	    <div>
	      <div style="padding-top: 5px; padding-bottom: 5px;">実行グループ登録</div>
	      <div class="display_div" style="width: 350px; vertical-align: top;">
	        <table class="list_procedure display" border="1" cellspacing="0" cellpadding="0" width="90%">
	          <thead>
	            <tr class="header">
	              <td>順序</td>
	              <td>グループ名</td>
	            </tr>
	          </thead>
	          <tbody>
	            <tr>
	              <td class="number"><a href="">1</a></td>
	              <td>メンテナンスサーバ1</td>
	            </tr>
	            <tr>
	              <td class="number"><a href="">2</a></td>
	              <td>メンテナンスサーバ1</td>
	            </tr>
	            <tr>
	              <td class="number"><a href="">3</a></td>
	              <td>メンテナンスサーバ1</td>
	            </tr>
	            <tr>
	              <td class="number"><a href="">4</a></td>
	              <td>メンテナンスサーバ1</td>
	            </tr>
	            <tr>
	              <td class="number"><a href="">5</a></td>
	              <td>メンテナンスサーバ1</td>
	            </tr>
	            <tr>
	              <td class="number"><a href="">6</a></td>
	              <td>メンテナンスサーバ1</td>
	            </tr>
	            <tr>
	              <td class="number"><a href="">7</a></td>
	              <td>メンテナンスサーバ1</td>
	            </tr>
	            <tr>
	              <td class="number"><a href="">8</a></td>
	              <td>メンテナンスサーバ1</td>
	            </tr>
	            <tr>
	              <td class="number"><a href="">9</a></td>
	              <td>メンテナンスサーバ1</td>
	            </tr>
	            <tr>
	              <td class="number"><a href="">10</a></td>
	              <td>メンテナンスサーバ1</td>
	            </tr>
	            <tr>
	              <td class="number"><a href="">11</a></td>
	              <td>メンテナンスサーバ1</td>
	            </tr>
	            <tr>
	              <td class="number"><a href="">12</a></td>
	              <td>メンテナンスサーバ1</td>
	            </tr>
	            <tr>
	              <td style="text-align: center;"><a href="">新規</a></td>
	              <td></td>
	            </tr>
	          </tbody>
	        </table>
	      </div>
	      <div class="display_div" style="width: 360px;">
	        <div>
	        	<div class="display_div">
			        <table width="100%" border="1">
			            <thead>
			              <tr class="title_head">
			                <td style="min-width: 50px;">順序</td>
			                <td>グループ名</td>
			              </tr>
			            </thead>
			            <tbody>
			              <tr>
			                <td class="number">1</td>
			                <td>
			                  <input class="edit_text" style="border:0;" type="text" name="" value="メンテナンスサーバ1">
			                </td>
			              </tr>
			            </tbody>
			          </table>
				</div>
				<div class="display_div" style="vertical-align: -webkit-baseline-middle;"><input style="margin-left: 20px;" id="btnSaveGroup" type="button" name="" value="Save group"></div>
	        </div>
	        <div style="margin-top: 20px;">
	          <div class="display_div">
	            <input class="edit_text" type="text" name="" value="1,2">
	          </div>
	          <div class="display_div"><input id="btnParentNoSelection" style="border: 1px solid; border-left: 0" type="button" name="" value="親番選択"></div>
	        </div>
	        <div id="tableContainer" class="tableContainer"  style="margin-top: 20px; width: 395px;">
	          <table class="table_scroll" width="100%" border="0" style="padding-top: 20px">
	            <thead>
	              <tr class="title_head">
	                <td style="width: 156px">パラメータ</td>
	                <td class="border_right" style="width: 220px">パラメータ値</td>
	              </tr>
	            </thead>
	            <tbody style="height: 157px">
	              <tr>
	                <td class="border_top_empty left" style="width: 156px">$serverName$</td>
	                <td class="border_top_empty border_right" style="width: 220px"><input class="edit_text" style=" border:0; width: 217px" type="text" name="" value="mnt01"></td>
	              </tr>
	              <tr>
	                <td class="left">$serverName$</td>
	                <td class="border_right"><input class="edit_text" style="border:0; width: 217px" type="text" name="" value="mnt01"></td>
	              </tr>
	              <tr>
	                <td class="left">$serverName$</td>
	                <td class="border_right"><input class="edit_text" style="border:0; width: 217px" type="text" name="" value="mnt01"></td>
	              </tr>
	              <tr>
	                <td class="left">$serverName$</td>
	                <td class="border_right"><input class="edit_text" style="border:0; width: 217px" type="text" name="" value="mnt01"></td>
	              </tr>
	            </tbody>
	          </table>
	        </div>
	        <div id="tableContainer" class="tableContainer" style="margin-top: 20px;">
	          <table class="list_procedure table_scroll" width="100%" border="0" style="padding-top: 20px">
	            <thead>
	              <tr class="title_head">
	                <td style="width: 43px">No.</td>
	                <td class="border_right" style="width: 333px">項目</td>
	              </tr>
	            </thead>
	            <tbody style="height: 240px">
	              <tr>
	                <td class="border_top_empty number" style="width: 44px">1</td>
	                <td class="border_top_empty border_right" style="width: 334px">LoginServer userID@mnt01passwd</td>
	              </tr>
	              <tr>
	                <td class="number">1</td>
	                <td class="border_right">LoginServer userID@mnt01passwd</td>
	              </tr>
	              <tr>
	                <td class="number">2</td>
	                <td class="border_right">LoginServer userID@mnt01passwd</td>
	              </tr>
	              <tr>
	                <td class="number">3</td>
	                <td class="border_right">LoginServer userID@mnt01passwd</td>
	              </tr>
	              <tr>
	                <td class="number">4</td>
	                <td class="border_right">LoginServer userID@mnt01passwd</td>
	              </tr>
	              <tr>
	                <td class="number">5</td>
	                <td class="border_right">LoginServer userID@mnt01passwd</td>
	              </tr>
	              <tr>
	                <td class="number">6</td>
	                <td class="border_right">LoginServer userID@mnt01passwd</td>
	              </tr>
	              <tr>
	                <td class="number">7</td>
	                <td class="border_right">LoginServer userID@mnt01passwd</td>
	              </tr>
	            </tbody>
	          </table>
	        </div>
	      </div>
	    </div>
    </div>
  </section>
</body>
</html>
