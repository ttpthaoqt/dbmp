<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>DB構築手順管理システム　【実行計画登録】</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="scripts/jquery-1.11.3.min.js"></script>
  <script src="scripts/jquery-ui.min.js"></script>
  <link rel="stylesheet" href="css/jquery-ui.css">
  <link rel="stylesheet" href="css/tablescroll.css">
  <link rel="stylesheet" href="css/style.css">
  
  <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <script type="text/javascript">
  $( function() {
	  var epid = "<c:out value="${epid}"/>";
	  var $radios = $('input:radio[name=radio_execution_plan_id]');
      $radios.filter('[value='+epid+']').prop('checked', true);
      $( "#datepicker_start" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        buttonText: "Select date"
      });
      $( "#datepicker_end" ).datepicker({
        showOn: "button",
        buttonImage: "images/calendar.gif",
        buttonImageOnly: true,
        buttonText: "Select date"
      });
      $( "#btnSelect" ).click(function() {
    	  var ptype = $('input[name=ptype]').val();
    	  var epid = $('input[name=radio_execution_plan_id]:checked', '#tableContainer').val();
    	  var gid = $('input[name=gid]').val();
    	  var perform = $('input[name=e]').val();
    	  var url = "${pageContext.request.contextPath}/work_execution?gid=" + gid + "&epid=" + epid + "&e=" + perform;
    	  if (ptype!=1){
    		  var url = "${pageContext.request.contextPath}/work_progress_confirmation?gid=" + gid + "&epid=" + epid;  
    	  }
    	  window.parent.$('.ui-dialog-content:visible').dialog('close');
    	  window.parent.location.href = url;
   	  });
      $( "#btnSearch" ).click(function() {
    	  document.getElementById("execution_plan_selection").submit();
   	  });
    } );
  </script>
</head>
<body>
  <section class="container" style="width: 720px; height: 575px">
    <h1>実行計画選択</h1>
    <form:form method="POST" id="execution_plan_selection" name="execution_plan_selection" action="${pageContext.request.contextPath}/execution_plan_selection" modelAttribute="ExecutionPlan">
    <form:errors path="" element="div"/>
    <input type="hidden" id="ptype" name="ptype" value="<c:out value="${ptype}"/>">
    <input type="hidden" id="epid" name="epid" value="<c:out value="${epid}"/>">
    <input type="hidden" id="gid" name="gid" value="<c:out value="${gid}"/>">
    <input type="hidden" id="e" name="e" value="<c:out value="${e}"/>">
    <div class="content" style="padding-top: 30px;">
      <div>
        <div class="display_div" style="width: 90%; vertical-align: top;">
          <table class="list_procedure display nowrap" border="1" cellspacing="2" cellpadding="2" width="95%">
            <tbody>
              <tr>
                <td class="header" style="text-align: center; width: 80px">実施予定日</td>
                <td style="width: 135px;">
                  <div class="display_div" style="width: 135px;">
                    <input id="datepicker_start" class="edit_text" style="width: 95px;" type="text" id="search_start_time" name="search_start_time" value="${dStartDate}">
                  </div>
                </td>
                <td style="text-align: center; width: 40px; border-top: 1px solid #fff">～</td>
                <td style="width: 135px;">
                  <div class="display_div" style="width: 135px;">
                    <input id="datepicker_end" class="edit_text" style="width: 95px;" type="text" id="search_end_time" name="search_end_time" value="${dEndDate}">
                  </div>
                </td>
              </tr>
              <tr style="height: 40px">
                <td class="header" style="text-align: center;">実施件名</td>
                <td colspan="3">
                	<input class="edit_text" style="width: 297px;" type="text" id="search_implemented_name" name="search_implemented_name" value='<c:out value="${search_implemented_name}"></c:out>'>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="display_div" style="vertical-align: -webkit-baseline-middle;">
          <div style="padding-bottom: 10px"><input type="button" id="btnSearch" name="btnSearch" value="検索"></div>
        </div>
        <div style="float: right; margin-top: 20px;"><input type="button" id="btnSelect" name="btnSelect" value="選択"></div>
      </div>
      <div id="tableContainer" class="tableContainer" style="margin-top: 20px;">
      	<table class="list_procedure table_scroll" width="88%" border="0">
          <thead>
          	<tr class="title_head">
              <td style="width: 41px;"></td>
              <td style="width: 158px;">実施予定日</td>
              <td style="width: 282px;">実施件名</td>
              <td class="border_right" style="width: 72px;">状況</td>
            </tr>
          </thead>
          <c:choose>
         	<c:when test="${fn:length(execution_plans) gt 8}">
         		<tbody style="height: 306px;">
         	</c:when>
         	<c:otherwise>
         		<tbody class="noscroll">
         	</c:otherwise>
          </c:choose>
          	<c:set var="i" scope="page" value="${0}"/>
            <c:forEach items="${execution_plans}" var="execution_plan">
            	<c:choose>
            		<c:when test="${i==0}">
	            		<tr>
	            		  <td class="border_top_empty center" style="width: 41px;">
	            		  	<input type="radio" checked id="radio_execution_plan_id" name="radio_execution_plan_id" value="<c:out value="${execution_plan.execution_plan_id}"/>">
            		  	  </td>
			              <td class="border_top_empty border_right" style="width: 158px;">
				              <c:choose>
				            	<c:when test="${not empty execution_plan.plan_date}">
				              		<fmt:formatDate pattern="yyyy/MM/dd H:m" value="${execution_plan.plan_date}" />
				             	</c:when>
				             	<c:otherwise>
				             		<c:out value=""></c:out>
				             	</c:otherwise>
				           	</c:choose>
			              </td>
			              <td class="border_top_empty border_right" style="width: 282px;">
			              	<c:out value="${execution_plan.implemented_name}"/>
			              </td>
			              <td class="border_top_empty border_right" style="width: 72px;">
			              	<c:if test="${execution_plan.status==0}">
				              	<c:out value="未実施 " />
				              </c:if>
				              <c:if test="${execution_plan.status==1}">
				              	<c:out value="実行中 " />
				              </c:if>
				              <c:if test="${execution_plan.status==2}">
				              	<c:out value="完了 " />
				              </c:if>
				              <c:if test="${execution_plan.status==3}">
				              	<c:out value="中断 " />
				              </c:if>
			              </td>
			            </tr>
	            	</c:when>
	            	<c:otherwise>
	            		<tr>
	            		  <td class="center">
	            		  	<input type="radio" id="radio_execution_plan_id" name="radio_execution_plan_id" value="<c:out value="${execution_plan.execution_plan_id}"/>">
	            		  </td>
			              <td>
			              	<c:out value="${execution_plan.plan_date}"/>
			              </td>
			              <td class="border_right">
			              	<c:out value="${execution_plan.implemented_name}"/>
			              </td>
			              <td class="border_right">
			              	<c:if test="${execution_plan.status==0}">
				              	<c:out value="未実施 " />
				              </c:if>
				              <c:if test="${execution_plan.status==1}">
				              	<c:out value="実行中 " />
				              </c:if>
				              <c:if test="${execution_plan.status==2}">
				              	<c:out value="完了 " />
				              </c:if>
				              <c:if test="${execution_plan.status==3}">
				              	<c:out value="中断 " />
				              </c:if>
			              </td>
			            </tr>
		            </c:otherwise>
            	</c:choose>
            	<c:set var="i" scope="page" value="${i+1}"/>
		    </c:forEach>
          </tbody>
        </table>
        </div>
    </div>
    </form:form>
  </section>
</body>
</html>
