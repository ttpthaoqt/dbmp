<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>DB構築手順管理システム　【作業実行】</title>
 <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="scripts/jquery-1.11.3.min.js"></script>
  <script src="scripts/jquery-ui.min.js"></script>
  <link rel="stylesheet" href="css/jquery-ui.css">
  <link rel="stylesheet" href="css/style.css">
    <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <script type="text/javascript">
    $( function() {
  	  var gid = "<c:out value="${gid}"/>";
	  var epid = "<c:out value="${epid}"/>";
	  var perform = "<c:out value="${e}"/>";
	  var hasdummy = "<c:out value="${hasdummy}"/>";
	  if (perform==1){
		  $('input[id=btnPerform]').prop('disabled', true);
	  }
	  if (hasdummy==1){
		  var newRowContent = '<tr>';
		  newRowContent += '<td class="number" style="width: 40px"></td>';
		  newRowContent += '<td style="width: 100px"></td>';
		  newRowContent += '<td></td>';
		  newRowContent += '<input type="hidden" id="idCopying" name="idCopying" value="">';
		  newRowContent += '<td id="idCopy" class="button_processing center"><input style="border: 0; font-weight: normal; padding-bottom: 1px;" type="button" name="" value="実行中"></td>';
		  newRowContent += '</tr>';
		  $("#tblExecutionPlanDetail > tbody:last-child").append(newRowContent);
	  }
      $(document).on('click', '#btnExecutionPlanSelection', function (e) {
          e.preventDefault();
          var page = "${pageContext.request.contextPath}/execution_plan_selection?ptype=1&gid=" + gid + "&epid=" + epid + "&e=" + perform;
          var $dialog = $('<div></div>')
          .html('<iframe src="' + page + '" width="100%" height="600"></iframe>')
          .dialog({
              resizable: false,
              height: 672,
              width: 800,
              modal: true,
              draggable: false,
              open: function(event,ui) {
                  $(this).parent().focus();
              }
          });
      });
      $(document).on('click', '#btnPerform', function (e) {
    	  var url = "${pageContext.request.contextPath}/work_execution?gid=<c:out value="${group_info.group_info_id}"/>&epid=<c:out value="${group_info.execution_plan_id}"/>&e=1";    
    	  $(location).attr('href',url);
      });
      
      $(document).on('click', '#idCopy', function (e) {
    	  var gid = $('#gid').val();
    	  var epid = $('#epid').val();
    	  var e = $('#e').val();
    	  var epdid = $('#processing').val();
    	  var epdidcopy = $('#idCopying').val();
    	  $.ajax({
              type: "POST",
              url: "${pageContext.request.contextPath}/work_execution_update",
              data: { gid: gid, epid: epid, e: e, epdid: epdid, epdidcopy: epdidcopy } ,
              success: function(response) {
            	  location.reload();
              }
          });
      });
      
    });
  </script>
</head>
<body>
  <section class="container">
   	<c:if test="${empty sessionScope.user_id}">
		<c:redirect url="/"></c:redirect>
	</c:if>
	<form:form method="POST" id="execution_plan_selection" name="execution_plan_selection" action="/work_execution_update" modelAttribute="ExecutionPlan">
    <form:errors path="" element="div"/>
    <input type="hidden" id="epid" name="epid" value="<c:out value="${epid}"/>">
    <input type="hidden" id="gid" name="gid" value="<c:out value="${gid}"/>">
    <input type="hidden" id="e" name="e" value="<c:out value="${e}"/>">
    <div class="navi_bar">
      <div class="title_bar" style="width: 72%;">DB構築手順管理システム　【作業実行】</div>
      <div class="title_bar" style="width: 110px;"><a href="${pageContext.request.contextPath}/top_menu">メニュー</a></div>
      <div class="title_bar">user:<c:out value="${sessionScope.user_name}"></c:out></div>
      <div class="title_bar"><a href="${pageContext.request.contextPath}/">Logout</a></div>
    </div>
    <div class="content">
    <div>
      <div class="display_div" style="width: 500px; vertical-align: top;">
        <table class="list_procedure display" border="1" cellspacing="0" width="90%">
          <thead>
            <tr class="header">
              <td>実施予定日</td>
              <td>実施件名</td>
              <td>状況</td>
            </tr>
          </thead>
          <tbody>
            <tr>
           	<c:choose>
            	<c:when test="${not empty execution_plan_detail.plan_date}">
              		<td><fmt:formatDate pattern="yyyy/MM/dd H:m" value="${execution_plan_detail.plan_date}" /></td>
             	</c:when>
             	<c:otherwise>
             		<td><c:out value=""></c:out></td>
             	</c:otherwise>
           	</c:choose>
              <td><c:out value="${execution_plan_detail.implemented_name}" /></td>
              <td class="center">
              <c:if test="${execution_plan_detail.status==0}">
              	<c:out value="未実施 " />
              </c:if>
              <c:if test="${execution_plan_detail.status==1}">
              	<c:out value="実行中 " />
              </c:if>
              <c:if test="${execution_plan_detail.status==2}">
              	<c:out value="完了 " />
              </c:if>
              <c:if test="${execution_plan_detail.status==3}">
              	<c:out value="中断 " />
              </c:if>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="display_div" style="padding-right: 20px; vertical-align: -webkit-baseline-middle;"><input id="btnExecutionPlanSelection" type="button" name="" value="計画選択"></div>
      <div class="display_div" style="vertical-align: -webkit-baseline-middle;"><input id="btnPerform" type="button" name="" value="実行"></div>
    </div>

    <div>
      <div class="display_div" style="width: 350px; vertical-align: top; padding-top: 20px;">
        <table class="list_procedure display" border="1" cellspacing="0" width="90%">
          <thead>
            <tr class="header">
              <td>実行グループ名</td>
              <td>状況</td>
            </tr>
          </thead>
          <tbody>
          	<c:forEach items="${group_infos}" var="group_info">
          		<tr>
          			<td><a href="${pageContext.request.contextPath}/work_execution?gid=<c:out value="${group_info.group_info_id}"/>&epid=<c:out value="${group_info.execution_plan_id}"/>">
          				<c:out value="${group_info.group_name}"/>
          			</a></td>
              		<td class="center"><c:out value="${group_info.ep_done}"/>/<c:out value="${group_info.ep_total}"/></td>
	            </tr>
		    </c:forEach>
          </tbody>
        </table>
      </div>
      <div class="display_div">
        <div style="margin-top: 20px; width: 250px;">
          <table class="list_procedure display nowrap" border="1" cellspacing="0" width="70%">
          <thead>
            <tr class="header">
              <td>実行グループ名</td>
            </tr>
          </thead>
          <tbody>
          <c:if test="${not empty group_info.group_name}">
          	<tr>
              <td>
              	<c:out value="${group_info.group_name}"></c:out>
              </td>
            </tr>
          </c:if>
          </tbody>
        </table>
        </div>
        <div style="margin-top: 20px; width: 400px">
          <table id="tblExecutionPlanDetail" class="list_procedure display nowrap" border="1" cellspacing="0" width="100%">
          <thead>
            <tr class="header">
              <td style="width:10%">No</td>
              <td style="width:30%">状況</td>
              <td style="width:40%">項目</td>
              <td style="width:20%"></td>
            </tr>
          </thead>
          <tbody>
          	<c:set var="i" scope="page" value="${1}"/>
          	<c:set var="copy" scope="page" value="${0}"/>
          	<c:forEach items="${execution_plan_details}" var="execution_plan_detail">
	            <tr id="ex_detail_<c:out value="${i}"/>">
	              <td class="number" style="width: 40px">
	              	<c:out value="${i}"/>
	              </td>
	              <c:choose>
	              	<c:when test="${(empty execution_plan_detail.start_time) && (empty execution_plan_detail.end_time)}">
	            	  <td style="width: 100px"></td>
			          <td id="item_command_<c:out value="${i}"/>">
			          	<c:out value="${execution_plan_detail.work_item_command}"/>
		              </td>
		              <c:choose>
			              <c:when test="${(e==1) && (copy==0)}">
		          			<c:set var="copy" scope="page" value="${i}"/>
		          			<input type="hidden" id="idCopying" name="idCopying" value="<c:out value="${execution_plan_detail.execution_plan_detail_id}"/>">
		          			<td id="idCopy" class="button_processing center"><input style="border: 0; font-weight: normal; padding-bottom: 1px;" type="button" name="" value="コピー"></td>
		          		  </c:when>
		          		  <c:otherwise>
		          		  	<td class="center"></td>
		          		  </c:otherwise>
	          		  </c:choose>
	            	</c:when>
	            	<c:when test="${(not empty execution_plan_detail.start_time) && (empty execution_plan_detail.end_time)}">
	            	  <input type="hidden" id="processing" name="processing" value="<c:out value="${execution_plan_detail.execution_plan_detail_id}"/>">
	            	  <td class="processing_status" style="width: 100px">実行中</td>
		              <td class="processing_status">
		              	<c:out value="${execution_plan_detail.work_item_command}"/>
		              </td>
		              <td class="button_processing center">
		              	<c:out value="実行中" />
		              </td>
	            	</c:when>
	            	<c:when test="${(not empty execution_plan_detail.start_time) && (not empty execution_plan_detail.end_time)}">
	            	  <td class="complete_status" style="width: 100px">実行済</td>
		              <td class="complete_status">
		              	<c:out value="${execution_plan_detail.work_item_command}"/>
		              </td>
		              <td class="center">
		              	<c:out value="完了" />
		              </td>
	            	</c:when>
	            	<c:otherwise>
		              	<td style="width: 100px"></td>
			            <td>
			              	<c:out value="${execution_plan_detail.work_item_command}"/>
		              	</td>
			            <td class="center">
			            </td>
		            </c:otherwise>
	              </c:choose>
	            </tr>
	            <c:set var="i" scope="page" value="${i+1}"/>
            </c:forEach>
          </tbody>
        </table>
        </div>
      </div>
    </div>
    </div>
    </form:form>
  </section>
</body>
</html>
