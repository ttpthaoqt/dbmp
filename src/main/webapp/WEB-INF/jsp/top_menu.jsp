<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>DB構築手順管理システム【メニュー】</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="scripts/jquery-1.11.3.min.js"></script>
  <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <style type="text/css">
  .disabled input{
  	background: darkgrey;
	color: gray;
  }
  </style>
</head>
<body>
  <section class="container">
  	<c:if test="${empty sessionScope.user_id}">
	<c:redirect url="/"></c:redirect>
	</c:if>
    <div class="navi_bar">
      <div class="title_bar" style="width: 81%;"><h1>DB構築手順管理システム【メニュー】</h1></div>
      <div class="title_bar">user:<c:out value="${sessionScope.user_name}"></c:out></div>
      <div class="title_bar"><a href="${pageContext.request.contextPath}/">Logout</a></div>      
    </div>
    <div class="content top_menu">
    <c:choose>
    <c:when test="${sessionScope.authority == 1}">
      <p class="button">
        <a href='${pageContext.request.contextPath}/procedure'><input class="top_menu_button" type="button" name="create_procedure" value="手順作成"></a>
      </p>
      <p class="button">
        <a href='${pageContext.request.contextPath}/execution_plan'><input class="top_menu_button" type="button" name="upgrade_execution_plan" value="実行計画登録"></a>
      </p>
      <p class="button">
        <a href="${pageContext.request.contextPath}/work_execution"><input class="top_menu_button" type="button" name="work_execution" value="作業実行"></a>
      </p>
      <p class="button disabled">
        <a href="javascript:;"><input class="top_menu_button" type="button" name="work_progress_confirmation" value="作業進捗確認"></a>
      </p>    
    </c:when>
    <c:when test="${sessionScope.authority == 2}">
      <p class="button disabled">
        <a href='javascript:;'><input class="top_menu_button" type="button" name="create_procedure" value="手順作成"></a>
      </p>
      <p class="button disabled">
        <a href='javascript:;'><input class="top_menu_button" type="button" name="upgrade_execution_plan" value="実行計画登録"></a>
      </p>
      <p class="button disabled">
        <a href="javascript:;"><input class="top_menu_button" type="button" name="work_execution" value="作業実行"></a>
      </p>
      <p class="button">
        <a href="${pageContext.request.contextPath}/work_progress_confirmation"><input class="top_menu_button" type="button" name="work_progress_confirmation" value="作業進捗確認"></a>
      </p>     
    </</c:when>
    </c:choose>
    </div>
  </section>
</body>
</html>
