<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>DB構築手順管理システム【ロ</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<script src="scripts/jquery-1.11.3.min.js"></script>

<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

<script>
$( function() {
  $(document).on('click', '#btnSubmit', function (e) {
  	$("#client-error-message").text("");	
  	var pid = $("#pid").val();
  	var parent_no = $("#parent_no").val();
  	var parent_name = $("#parent_name").val();
  	
  	var errorMsg;
    var regex=/^[0-9]+$/;
    if((parent_no === null || parent_no === '') || (parent_name === null || parent_name === '')){
    	errorMsg = "<spring:message code="input_required"/>";
        $("#client-error-message").text(errorMsg);
    	$("#client-error-message").css("display", "block");
    	$("#error-message").css("display", "none");
        return false;
    }
    else if (!parent_no.match(regex) || parent_no > 2147483647 || parent_no <= 0)
    {
    	errorMsg = "<spring:message code="create_parent.input_number_invalid"/>";
        $("#client-error-message").text(errorMsg);
    	$("#client-error-message").css("display", "block");
    	$("#error-message").css("display", "none");
        return false;
    }else{
    	$.ajax({
	       type: "POST",
	       url: "create_parent",
	       data: { pid: pid, parent_no: parent_no, parent_name: parent_name } ,
	       success: function(response) {
	     	  parent.location.reload();
	     	  window.parent.$('.ui-dialog-content:visible').dialog('close');
	       }
		});
    }
  });
  
  $("#parent_no").keypress(function(e) {
	  if(e.which < 48 || e.which > 57) {
		  e.preventDefault();
	  }
  });
});
</script>
<style>
#client-error-message{
	display: none;
	color: red;
}

#error-message{
	color: red;
}
</style>
</head>
<body>
  	<c:if test="${empty sessionScope.user_id}">
	<c:redirect url="/"></c:redirect>
	</c:if>
	<section class="container" style="width: 400px; height: 250px">
		<h1>親手順作成</h1>
		<div class="content">
			<div>
				<table border="0" cellspacing="0" width="95%">
					<tbody>
						<tr>
							<td style="float: right;"><input type="submit" name=""
								id="btnSubmit" value="作成"></td>
						</tr>
					</tbody>
				</table>
			</div>

			<form:form method="POST" action="/create_parent" id="form-submit">
				<input type="hidden" id="pid" name="pid" value="<c:out value="${parentInfo.parent_info_id}"/>">
				<c:if test="${not empty errorMessage}">
					<span id="error-message"><spring:message code="${errorMessage}"/></span>
				</c:if>
				<span id="client-error-message"></span>
				<div class="display_div"
					style="width: 400px; vertical-align: top; padding-top: 20px;">
					<div>
						<div class="display_div title_head label_left" style="width: 91px; border-bottom: 0;">親手順No</div>
						<div class="display_div">
							<input id="parent_no" type="text" maxlength="9"  class="edit_text" style="width: 60px; border-bottom: 0;"
								name="parent_no" onkeypress="inputNumericOnly()" value="<c:out value="${parentInfo.parent_no}"></c:out>">
						</div>
					</div>
					<div>
						<div class="display_div title_head label_left" style="width: 91px;">親手順名称</div>
						<div class="display_div">
							<input id="parent_name" class="edit_text" style="width: 220px;" type="text"
								name="parent_name" value="<c:out value="${parentInfo.parent_name}"></c:out>">
						</div>
					</div>
				</div>
			</form:form>
		</div>
	</section>
</body>
</html>
