<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>DB構築手順管理システム【ロ</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<script src="scripts/jquery-1.11.3.min.js"></script>
<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

<script type="text/javascript">
$( function() {
	var parent_id = "<c:out value="${childInfo.parent_id}"></c:out>";
	$("select#parent_id").val(parent_id);
	$(document).on('click', '#btnSubmit', function(e) {
		var cid = $("#cid").val();
		var parent_id = $("select#parent_id").val();
		var child_order = $("#child_order").val();
		var work_item = $("#work_item").val();
		var remarks = $("#remarks").val();
		var errorMsg;
		var regex = /^[0-9]+$/;
		if ((parent_id === null || parent_id === '') || (child_order === null || child_order === '')
				  || (work_item === null || work_item === '')) {
			errorMsg = "<spring:message code="input_required"/>";
			$("#client-error-message").text(errorMsg);
			$("#client-error-message").css("display", "block");
			$("#error-message").css("display", "none");
			return false;
		} else if (!child_order.match(regex) || child_order > 2147483647 || child_order <= 0) {
			errorMsg = "<spring:message code="create_child.input_number_invalid"/>";
			$("#client-error-message").text(errorMsg);
			$("#client-error-message").css("display", "block");
			$("#error-message").css("display", "none");
			return false;
		} else {
			$.ajax({
		       type: "POST",
		       url: "create_child",
		       data: { cid: cid, parent_id: parent_id, child_order: child_order, work_item: work_item, remarks: remarks } ,
		       success: function(response) {
		     	  parent.location.reload();
		     	  window.parent.$('.ui-dialog-content:visible').dialog('close');
		       }
			});
		}
	});
	
	$("#child_order").keypress(function(e) {
	  if(e.which < 48 || e.which > 57) {
		  e.preventDefault();
	  }
  	});
} );
</script>
<style>
#client-error-message {
	display: none;
	color: red;
}

#error-message {
	color: red;
}
</style>
</head>
<body>
  <section class="container" style="width: 410px; height: 355px">
  	<form:form method="POST" id="create_child" name="create_child" action="/create_child" modelAttribute="ChildInfo">
  	<input type="hidden" id="cid" name="cid" value="<c:out value="${childInfo.child_info_id}"/>">
  	<%-- <form:errors path="" element="div"/> --%>
    <h1>子手順作成</h1>
    <div class="content">
      <div>
        <table border="0" cellspacing="0" width="100%">
          <tbody>
            <tr>
              <td style="float: right;"><input id="btnSubmit" name="btnSubmit" type="button" name="" value="作成"></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div style="width: 340px; vertical-align: top; padding-top: 20px;">
      	<table class="table_scroll" width="100%" border="1"">
            <thead>
              <tr class="title_head">
                <td style="width: 156px">パラメータ</td>
                <td class="border_right" style="width: 220px">パラメータ値</td>
              </tr>
            </thead>
            <tbody class="noscroll" style="height: 157px">
              <c:if test="${not empty errorMessage}">
              	<span id="error-message"><spring:message code="${errorMessage}"/></span>
				<%-- <tr>
	              	<td colspan="2">
	              		<span id="error-message"><spring:message code="${errorMessage}"/></span>
	              	</td>
	            </tr> --%>
			  </c:if>
			  <span id="client-error-message" style="padding-bottom: 20px;"></span>
              <tr>
                <td class="border_top_empty left" style="width: 90px">親手順</td>
                <td class="border_top_empty border_right" style="width: 224px">
					<select id="parent_id" name="parent_id" class="dropdown-select edit_text" style="width: 224px;">
			            <option value=""></option>
			            <c:forEach items="${listParentInfos}" var="parent_info">
			              <option value="<c:out value="${parent_info.parent_info_id}"></c:out>"><c:out value="${parent_info.parent_name}"></c:out></option>
			            </c:forEach>
		            </select>
				</td>
              </tr>
              <tr>
                <td class="left">子手順順序</td>
                <td class="border_right">
                	<input id="child_order" name="child_order" maxlength="4" class="edit_text" style="width: 224px; border:0" type="text" value='<c:out value="${childInfo.child_order}"></c:out>'>
                </td>
              </tr>
              <tr>
                <td class="left">手順項目</td>
                <td class="border_right">
					<input id="work_item" name="work_item" class="edit_text" style="width: 224px; border:0" type="text" name="" value="<c:out value="${childInfo.work_item}"></c:out>">
				</td>
              </tr>
              <tr>
                <td class="left">備考</td>
                <td class="border_right">
                	<input id="remarks" name="remarks" class="edit_text" style="width: 224px; border:0" type="text" name="" value="<c:out value="${childInfo.remarks}"></c:out>">
                </td>
              </tr>
            </tbody>
          </table>
      </div>
      <%-- <div style="width: 400px; vertical-align: top; padding-top: 10px;">
        <div>
        <c:if test="${not empty errorMessage}">
			<span id="error-message"><spring:message code="${errorMessage}"/></span>
		</c:if>
		<span id="client-error-message" style="padding-bottom: 20px;"></span>
          <div class="display_div title_head" style="text-align: left !important; width: 90px; height: 36px; border-top: 1px solid; border-right: 0; border-left: 1px solid; border-bottom: 0; padding-left: 2px">親手順</div>
          <div class="display_div" style="width: 224px;">
            <select id="parent_id" name="parent_id" class="dropdown-select edit_text" style="height: 36px; width: 224px; border: 1px solid; border-bottom: 0">
            <option value=""></option>
            <c:forEach items="${listParentInfos}" var="parent_info">
              <option value="<c:out value="${parent_info.parent_info_id}"></c:out>"><c:out value="${parent_info.parent_name}"></c:out></option>
            </c:forEach>
            </select>
          </div>
        </div>
      </div>
      <div style="width: 400px; vertical-align: top;">
        <div>
          <div class="display_div title_head" style="text-align: left !important; width: 91px; height: 36px; border-top: 1px solid; border-right: 1px solid; border-left: 1px solid; border-bottom: 0; padding-left: 2px">子手順順序</div>
          <div class="display_div edit_text style="width: 220px; height: 36px; border-top: 1px solid; border-right: 1px solid; border-bottom: 0; padding-left: 2px"">
          	<input id="child_order" name="child_order" class="edit_text" style="height: 34px; width: 220px; border:0" type="text" value='<c:out value="${childInfo.child_order}"></c:out>'>
          </div>
        </div>
      </div>
      <div style="width: 400px; vertical-align: top;">
        <div>
          <div class="display_div title_head" style="text-align: left !important; width: 91px; height: 36px; border-top: 1px solid; border-right: 1px solid; border-left: 1px solid; border-bottom: 0; padding-left: 2px">手順項目</div>
          <div class="display_div edit_text" style="width: 220px; height: 36px; border-top: 1px solid; border-right: 1px solid; padding-left: 2px">
          <input id="work_item" name="work_item" class="edit_text" style="height: 34px; width: 220px; border:0" type="text" name="" value="<c:out value="${childInfo.work_item}"></c:out>">
          </div>
        </div>
      </div>
      <div style="width: 400px; vertical-align: top;">
        <div>
          <div class="display_div title_head" style="text-align: left !important; width: 91px; height: 36px; border-top: 1px solid; border-right: 1px solid; border-left: 1px solid; border-bottom: 1px solid; padding-left: 2px">備考</div>
          <div class="display_div edit_text" style="width: 220px; height: 36px; border-top: 1px solid; border-right: 1px solid; border-bottom: 1px solid; padding-left: 2px">
          <input id="remarks" name="remarks" class="edit_text" style="height: 34px; width: 220px; border:0" type="text" name="" value="<c:out value="${childInfo.remarks}"></c:out>">
          </div>
        </div>
      </div> --%>
    </div>
    </form:form>
  </section>
</body>
</html>
