<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>DB構築手順管理システム　【手順作成】</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="css/responsive.dataTables.min.css">
  <script src="scripts/jquery-1.11.3.min.js"></script>
  <script src="scripts/jquery.dataTables.min.js"></script>
  <script src="scripts/dataTables.responsive.min.js"></script>
  <script src="scripts/jquery-ui.min.js"></script>
  <link rel="stylesheet" href="css/jquery-ui.css">
  
  <script type="text/javascript">
    $(document).ready(function() {
      // Standard initialisation
      $('#list_procedure').DataTable( {
          responsive: true,
          "searching": false,
          "ordering": false,
          paging: true,
          "language": {
        	  "sEmptyTable":     "テーブルにデータがありません",
        	  "sInfo":           " _TOTAL_ 件中 _START_ から _END_ まで表示",
        		"sInfoEmpty":      " 0 件中 0 から 0 まで表示",
        		"sInfoFiltered":   "（全 _MAX_ 件より抽出）",
        	  "sInfoPostFix":    "",
        	  "sInfoThousands":  ",",
        		"sLengthMenu":     "_MENU_ 件表示",
        	  "sLoadingRecords": "読み込み中...",
        		"sProcessing":     "処理中...",
        	  "sSearch":         "検索:",
        		"sZeroRecords":    "一致するレコードがありません",
        		"oPaginate": {
        			"sFirst":    "先頭",
        			"sLast":     "最終",
        			"sNext":     "次",
        			"sPrevious": "前"
        		},
        	  "oAria": {
        			"sSortAscending":  ": 列を昇順に並べ替えるにはアクティブにする",
        			"sSortDescending": ": 列を降順に並べ替えるにはアクティブにする"
        		}
        	},
          "bLengthChange": false
      } );         
    } );
    
    $(document).on('click', '#btnChild', function (e) {
        e.preventDefault();
        var page = "${pageContext.request.contextPath}/create_child";
        var $dialog = $('<div></div>')
        .html('<iframe src="' + page + '" width="100%" height="405px"></iframe>')
        .dialog({
            resizable: false,
            height: 477,
            width: 505,
            modal: true,
            draggable: false,
            open: function(event,ui) {
                $(this).parent().focus();
            }
        });
    });
    
    $(document).on('click','#btnParent', function (e) {
        e.preventDefault();
        var page = "${pageContext.request.contextPath}/create_parent";
        var $dialog = $('<div></div>')
        .html('<iframe src="' + page + '" width="100%" height="280px"></iframe>')
        .dialog({
            resizable: false,
            height: 352,
            width: 500,
            modal: true,
            draggable: false,
            open: function(event,ui) {
                $(this).parent().focus();
            }
        });
    });
    
    $(document).on('click','#aChild', function (e) {
        e.preventDefault();
        var cid = $(this).attr("value");
        var page = "${pageContext.request.contextPath}/create_child?cid=" + cid;
        var $dialog = $('<div></div>')
        .html('<iframe src="' + page + '" width="100%" height="405px"></iframe>')
        .dialog({
            resizable: false,
            height: 477,
            width: 505,
            modal: true,
            draggable: false,
            open: function(event,ui) {
                $(this).parent().focus();
            }
        });
    });
    
    $(document).on('click','#aParent', function (e) {
        e.preventDefault();
        var pid = $(this).attr("value");
        var page = "${pageContext.request.contextPath}/create_parent?pid=" + pid;
        var $dialog = $('<div></div>')
        .html('<iframe src="' + page + '" width="100%" height="280px"></iframe>')
        .dialog({
            resizable: false,
            height: 352,
            width: 500,
            modal: true,
            draggable: false,
            open: function(event,ui) {
                $(this).parent().focus();
            }
        });
    });
  </script>
  <link rel="stylesheet" href="css/style.css">
  <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
  <section class="container">
  	<c:if test="${empty sessionScope.user_id}">
		<c:redirect url="/"></c:redirect>
	</c:if>
    <div class="navi_bar">
      <div class="title_bar" style="width: 72%;"><h1>DB構築手順管理システム　【手順作成】</h1></div>
      <div class="title_bar" style="width: 110px;"><a href="${pageContext.request.contextPath}/top_menu">メニュー</a></div>
      <div class="title_bar">user:<c:out value="${sessionScope.user_name}"></c:out></div>
      <div class="title_bar"><a href="${pageContext.request.contextPath}/">Logout</a></div>     
    </div>
    <div class="content">
    <div>
    <form action="${pageContext.request.contextPath}/procedure" method="POST" >
      <div class="display_div title_head label_left" style="width: 100px;">親手順名称</div>
      <div class="display_div" style="width: 216px;">
      	<form:select id="ddl-parentinfo" class="dropdown-select edit_text" style="height: 38px; border: 1px solid" path="parentInfo" name="selectedParentInfo">
      	<option value="-1"></option> 
		<c:forEach items='${parentInfo}' var='parentInfo'> 
		<c:choose> 
		<c:when test="${parentInfo.key eq parentInfoId}"> 
		<option value="${parentInfo.key}" selected="selected">${parentInfo.value}</option> 
		</c:when> 
		<c:otherwise> 
		<option value="${parentInfo.key}">${parentInfo.value}</option> 
		</c:otherwise> 
		</c:choose> 
		</c:forEach> 
      	      	
      	</form:select>
      </div>
      <div class="display_div title_head label_left" style="width: 100px;">子手順項目</div>
      <div class="display_div" style="width: 200px; padding-right: 10px"><input class="edit_text" type="text" name="workItem" value="${workItem}"></div>
      <div class="display_div" style="width: 100px;"><input type="submit" name="" value="検索"></div>         
      <div class="display_div" style="width: 150px;"><input type="button" name="" id="btnParent" value="新親手順作成"></div>
      <div class="display_div"><input type="button" name="" id="btnChild" value="新小手順作成"></div>
    </form>
    </div>

    <div>
      <table id="list_procedure" class="cell-border" cellspacing="0" width="100%" border="1" style="margin-top: 20px">
        <thead>
          <tr class="header">
            <th class="title_head" colspan="2">親手順</th>
            <th class="title_head" colspan="3">子手順</th>
          </tr>
          <tr class="header">
            <th class="title_head">No.</th>
            <th class="title_head">名称</th>
            <th class="title_head">No.</th>
            <th class="title_head">項目</th>
            <th class="title_head">備考</th>
          </tr>
        </thead>
        <tbody>
        	<c:set var="i" scope="page" value="${1}"/>
        	<c:set var="parent_id_tmp" scope="page" value=""/>
          	<c:forEach items="${parentChildInf}" var="parentChildInf">
          	  <c:choose>
          	  	<c:when test="${(parent_id_tmp!=parentChildInf.parent_info_id)}">
          	  		<c:set var="i" scope="page" value="${1}"/>
          	  	</c:when>
          	  </c:choose>
	          <tr>
	            <td class="number" style="border-left-color: #000"><a id="aParent" value="<c:out value="${parentChildInf.parent_info_id}"/>" href="#"><c:out value="${parentChildInf.parent_no}"/></a></td>
	            <td><c:out value="${parentChildInf.parent_name}"/></td>
	            <td class="number">
	            <c:choose>
	          	  	<c:when test="${(not empty parentChildInf.child_info_id)}">
	          	  		<a id="aChild" value="<c:out value="${parentChildInf.child_info_id}"/>" href="#"><c:out value="${i}"/></a>
	          	  	</c:when>
	          	</c:choose>
	            </td>
	            <td><c:out value="${parentChildInf.work_item}"/></td>
	            <td><c:out value="${parentChildInf.remarks}"/></td>
	          </tr>	
	          <c:set var="i" scope="page" value="${i+1}"/>
	          <c:set var="parent_id_tmp" scope="page" value="${parentChildInf.parent_info_id}"/>            
		    </c:forEach>        
        </tbody>
      </table>
    </div>
    </div>
  </section>
</body>
</html>
